//
//  MyTest.m
//  MyTest
//
//  Created by fei wang on 4/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "MyTest.h"

#import "RestClient.h"

@implementation MyTest{
    RestClient *_client;
}

- (void)setUp
{
    [super setUp];
    
    _client = [[RestClient alloc] initRequest:@"http://pinterest.back/rest/api/index?method=saveTrip" method:@"POST" timeout:60];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testNormalPost
{
    //STFail(@"Unit tests are not implemented yet in MyTest");
    
    NSMutableData *data = [[NSMutableData alloc] init];
    
    BOOL result = [_client sendPostRequest:data];
    
    STAssertTrue(result, @"Didn't send out request!");
    
    NSString *returnStr = [_client getData];
    
    STAssertNotNil(returnStr, @"服务器没有返回结果");
}

@end
