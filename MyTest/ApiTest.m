//
//  ApiTest.m
//  Olutu
//
//  Created by fei wang on 22/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ApiTest.h"

#import "Api.h"

#import "Data.h"

@implementation ApiTest{
    Api *api;
}

- (void)setUp
{
    [super setUp];
    
    api = [[Api alloc] init];
    
    
}




- (void)testSaveTrip{
    
    DataTrip *trip = [[DataTrip alloc] init];
    DataShareSetting *shareSetting = [[DataShareSetting alloc] init];
    
    shareSetting.weibo = [NSNumber numberWithBool:YES];
    shareSetting.qq = [NSNumber numberWithBool:NO];
    shareSetting.facebook = [NSNumber numberWithBool:YES];
    shareSetting.twitter = [NSNumber numberWithBool:NO];
    
    trip.Share = shareSetting;
    
    trip.Name = @"Test Trip from unit test";
    
    trip.isGroup = [NSNumber numberWithBool:YES];
    
    trip.isLocked = [NSNumber numberWithBool:YES];
    
    NSMutableArray *groupMembers = [[NSMutableArray alloc] initWithCapacity:0];
    
    [groupMembers addObject:[NSNumber numberWithInt:1]];
    
    [groupMembers addObject:[NSNumber numberWithInt:2]];
    
    [groupMembers addObject:[NSNumber numberWithInt:3]];
    
    trip.groupMembers = groupMembers;
    
    NSDictionary *result = [api saveTrip:trip];
    
    STAssertNotNil(result, @"没有结果");
    
}


- (void)tearDown
{
    [super tearDown];
}

@end
