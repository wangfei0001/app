//
//  XMLParserTest.m
//  coolTab
//
//  Created by fei wang on 4/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "XMLParserTest.h"

#import "XMLParser.h"

@implementation XMLParserTest{
    XMLParser *xmlParser;
    
    NSString *xmlContents;
    
}

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.    
    NSString *fileName = @"response.xml";
    
    
    //NSString *fullPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"response.xml"];
    NSString *fullPath = [[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:fileName];
    
    
    NSFileManager *file_manager = [NSFileManager defaultManager];
    bool ifexist = [file_manager fileExistsAtPath:fullPath];

    STAssertTrue(ifexist, @"Resource file not exist");
    
    NSError *error = nil;
    NSStringEncoding encoding;
    
    xmlContents = [NSString stringWithContentsOfFile:fullPath usedEncoding:&encoding
                                                      error:&error];
    STAssertNotNil(xmlContents, @"Response file is empty");


}

- (void)tearDown
{
    // Tear-down code here.
    
    
    [super tearDown];
}

- (void)testExample
{
    xmlParser = [[XMLParser new]initWithString:xmlContents];
    
    STAssertNotNil(xmlParser, @"Could not create test subject.");
    
    [xmlParser parse];
    
   
    Boolean result = [xmlParser isSuccess];
    STAssertTrue(result, @"Can not get response from xml.");
    
    RestResponse *response = [xmlParser getResponse];
    STAssertNotNil(response, @"Response is null");
    
    NSDictionary *data = [response getData];
    NSLog(@"Dersialized JSON Dictionary = %@", data);
    STAssertNotNil(data, @"User object is null");
    
    
    //STFail(@"Unit tests are not implemented yet in MyTest");
}

@end
