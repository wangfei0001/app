//
//  AppDelegate.h
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Global.h"

#import "Session.h"

#import "MBProgressHUD.h"

#import "SinaWeibo.h"

#import "SinaWeiboRequest.h"

#define SHOW_COVER YES;

#define APP_DELEGATE (AppDelegate*)[UIApplication sharedApplication].delegate

@class viewController;

@class SinaWeibo;

@class User;

@class Database;

@class FacebookClient;

User *myself;

Database *db;

Global *global;




@interface AppDelegate : UIResponder <UIApplicationDelegate/*, SinaWeiboDelegate, SinaWeiboRequestDelegate*/>

+ (void) hideTabBar:(UITabBarController *) tabbarcontroller;

+ (void) showTabBar:(UITabBarController *) tabbarcontroller;

- (void)ShowLoading: (UIView *)view;

- (void)HideLoading;

@property (strong, nonatomic) SinaWeibo *sinaweibo;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UIViewController *viewController;

@property (strong, nonatomic) FacebookClient *facebookClient;

@property (strong, nonatomic) Session *session;

@property (strong, nonatomic) MBProgressHUD *HUD;

@end
