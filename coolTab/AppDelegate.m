//
//  AppDelegate.m
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import "AppDelegate.h"

#import "UsersController.h"

#import "CreateTripController.h"

#import "MyTripController.h"

#import "Global.h"

#import "SinaWeibo.h"

#import "User.h"

#import "Database.h"

#import "DefaultController.h"

#import "FacebookClient.h"

#import "Session.h"

#import "GAI.h"

#import "Api.h"

#import "Image.h"

@implementation AppDelegate

@synthesize sinaweibo;

@synthesize session;


//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    // attempt to extract a token from the url
//    BOOL authResult = [FBAppCall handleOpenURL:url
//                             sourceApplication:sourceApplication
//                                   withSession:self.facebookClient.session];
//    
////    if(self.facebook.session.isOpen){
////        NSLog(@"open url success!");
////        [self.facebook getUserInfo];
////    }else{
////        NSLog(@"open url failed!");
////    }
//    
//    return authResult;
//}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"elinor0001", @"username",
                                @"elinor0001@hotmail.com", @"email",
                                //                                @"fei", @"fname",
                                //                                @"wang", @"lname",
                                @"616682", @"password",
                                nil];
    
    NSURL *url = [NSURL URLWithString:@"http://olutu-yii/"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient clearAuthorizationHeader];
    [httpClient setAuthorizationHeaderWithUsername:@"wangfei0001" password:@"616682"];
    
    NSData *imageData = UIImageJPEGRepresentation([UIImage imageNamed:@"IMG_0998.JPG"], 0.5);
    NSMutableURLRequest *request = [httpClient multipartFormRequestWithMethod:@"POST" path:@"api/v1/trips/1/tripPost" parameters:parameters constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        [formData appendPartWithFileData:imageData name:@"photo[0]" fileName:@"MainMedia.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFileData:imageData name:@"photo[1]" fileName:@"MainMedia.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFileData:imageData name:@"photo[2]" fileName:@"MainMedia.jpg" mimeType:@"image/jpeg"];        
    }];
    
    
    AFJSONRequestOperation *operation = [[AFJSONRequestOperation alloc] initWithRequest:request];
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
         NSLog(@"Sent %lld of %lld bytes", totalBytesWritten, totalBytesExpectedToWrite);
    }];
    
    
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"%@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", [error localizedDescription]);
    }];
    [operation start];
    //global = (Global *)[Global getInstance];
    


    
    //显示Table界面
//    self.viewController = [[UsersController alloc] initWithNibName:@"UsersController" bundle:nil];
//    self.window.rootViewController = self.viewController;
//    [self.window makeKeyAndVisible];
    
//    UIImage *navBackgroundImage = [UIImage imageNamed:@"head.jpeg"];
//    [[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
//
//
    
//    Image *imageModel = [Image alloc];
//    
//    [imageModel getImageInfo];

    
    //init myself, database related!!!
#if 0
    myself = [[User alloc]init];
    [myself setIdUser:1];
    [myself setGender:YES];
    [myself setAvatar:@"head.jpeg"];
    
    db =  [Database database];
    NSArray *trips = [db getTrips];
#endif
    UITabBar *tabbar = [UITabBar appearance];
    [tabbar setShadowImage:[UIImage imageNamed:@"transparentShadow.png"]];
    
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 2;
    
    //[DBManager Init];
    
    self.facebookClient = [[FacebookClient alloc] init];
    
    self.session = [Session instance];
    

    
//    // Optional: automatically send uncaught exceptions to Google Analytics.
//    [GAI sharedInstance].trackUncaughtExceptions = YES;
//    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
//    [GAI sharedInstance].dispatchInterval = 20;
//    // Optional: set debug to YES for extra debugging information.
//    [GAI sharedInstance].debug = YES;
//    // Create tracker instance.
//    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-42093929-1"];
    

    
    return YES;

}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [self.sinaweibo handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    // attempt to extract a token from the url
    BOOL authResult = [FBAppCall handleOpenURL:url
                             sourceApplication:sourceApplication
                                   withSession:self.facebookClient.session];
    
    if(self.facebookClient.session.isOpen){
        NSLog(@"open url success!");
        //        if([[Session instance] isGuest]){
        
        //[self.facebookClient getUserInfo];
        //        }else{
        //            [self.facebook.delegate authorisedDone:YES];
        //        }
    }else{
        NSLog(@"open url failed!");
    }
    
    return authResult;
    
    //return [self.sinaweibo handleOpenURL:url];
}


+ (void) showTabBar:(UITabBarController *) tabbarcontroller {

    int height = 436;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0];
    
    for(UIView *view in tabbarcontroller.view.subviews) {
        if([view isKindOfClass:[UITabBar class]]) {
            [view setFrame:CGRectMake(view.frame.origin.x, height, view.frame.size.width, view.frame.size.height)];
        }
        else {
            [view setFrame:CGRectMake(view.frame.origin.x,view.frame.origin.y, view.frame.size.width, height)];
        }
    }
    [UIView commitAnimations];
    tabbarcontroller.tabBar.hidden = NO;
}

+ (void) hideTabBar:(UITabBarController *) tabbarcontroller {
    
    int height = 480;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0];
    
    for(UIView *view in tabbarcontroller.view.subviews) {
        
        if([view isKindOfClass:[UITabBar class]]) {
            [view setFrame:CGRectMake(view.frame.origin.x, height, view.frame.size.width, view.frame.size.height)];
        }
        else {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, height)];
        }
    }
    
    [UIView commitAnimations];
    tabbarcontroller.tabBar.hidden = YES;
}


- (void)ShowLoading: (UIView *)view
{
    if(self.HUD != nil) [self HideLoading];
    
    self.HUD = [[MBProgressHUD alloc] initWithView:view];
    [view addSubview:self.HUD];
    [self.HUD show:YES];
}

- (void)HideLoading
{
    [self.HUD removeFromSuperview];
    self.HUD = nil;
}

@end
