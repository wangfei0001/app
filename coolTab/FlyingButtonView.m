//
//  FlyingButtonView.m
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import "FlyingButtonView.h"

//#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FlyingButtonView()

@property(nonatomic, strong) UIImage *normalImage;
@property(nonatomic) CGPoint startPoint;
@property(nonatomic) CGPoint endPoint;

@end


@implementation FlyingButtonView

- (FlyingButtonView *)initView:(NSString *)title
                     normalImage:(UIImage *)normalImage
                    startPoint: (CGPoint) startPoint
                      endPoint: (CGPoint) endPoint
                    delegate: (id <FlyingButtonViewDelegate>) delegate{
    
    self = [self initWithImage: normalImage
                                       highlightedImage: normalImage];
    if(self){
        self.normalImage = normalImage;
        self.startPoint = startPoint;
        self.endPoint = endPoint;
        
        //显示图像

        self.frame = CGRectMake(endPoint.x,
                                          endPoint.y,
                                          self.normalImage.size.width,
                                          self.normalImage.size.height);
        
        self.userInteractionEnabled = YES;
    }
    
    
    return self;
}


//Rotate the view layer
- (void)_rotate:(CALayer *)layer
       duration: (float)duration{
    CABasicAnimation* rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotateAnimation.duration = duration;
    rotateAnimation.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear];
    rotateAnimation.cumulative = YES;
    rotateAnimation.toValue = [NSNumber numberWithFloat:4 * 2 * M_PI];
    [layer addAnimation:rotateAnimation forKey:@"rotateAnimation"];
}

- (void)expand:(BOOL)animation{
    //NSLog(@"Start to expand");
    [self _rotate:self.layer duration:0.2];
    
    NSTimeInterval duration = animation ? 0.2 : 0;
    
    [UIView animateWithDuration:duration delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                        animations:^void(){
                            self.frame = CGRectMake(
                                                      self.startPoint.x,
                                                      self.startPoint.y,
                                                      self.normalImage.size.width,
                                                      self.normalImage.size.height
                                                  );
                        }
                                                                  
                     completion:^void(BOOL finished){
                         
                     }];
}

- (void)collapse:(BOOL)animation{

    [self _rotate:self.layer duration:0.2];

    NSTimeInterval duration = animation ? 0.2 : 0;
    
    [UIView animateWithDuration:duration delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                        animations:^void(){
                            self.frame = CGRectMake(
                                                      self.endPoint.x,
                                                      self.endPoint.y,
                                                      self.normalImage.size.width,
                                                      self.normalImage.size.height
                                                  );
                        }
     
                     completion:^void(BOOL finished){
                         
                     }];
    
}



@end
