//
//  main.m
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
