//
//  CoverController.m
//  coolTab
//
//  Created by fei wang on 18/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import "CoverController.h"
#import "ViewController.h"

#import <QuartzCore/QuartzCore.h>

#import "Trip.h"

@interface CoverController ()

@end

@implementation CoverController



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view addSubview:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_pattern_wood.png"]]];
    
#ifndef SHOW_COVER
    //fView =[[UIImageView alloc]initWithFrame:self.window.frame];//初始化fView
    //fView.image=[UIImage imageNamed:@"f.png"];//图片f.png 到fView
    
    zView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_pattern_wood.png"]];//初始化zView
    //zView.image=[UIImage imageNamed:@"z.png"];//图片z.png 到zView
    
    rView=[[UIView alloc]initWithFrame:self.view.frame];//初始化rView
    
    //[rView addSubview:fView];//add 到rView
    [rView addSubview:zView];//add 到rView
    
    
    [self.view addSubview:rView];//add 到window
    
    [self performSelector:@selector(TheAnimation) withObject:nil afterDelay:0];//5秒后执行TheAnimation
#endif

}


- (void)TheAnimation{
    ViewController * mainViewController = (ViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"mainController"];
    [mainViewController setTripData:[Trip loadTrips]];
    
    UINavigationController *navigationController = (UINavigationController *)[self.storyboard instantiateViewControllerWithIdentifier:@"navigationcontroller"];
    id nav = [navigationController initWithRootViewController:mainViewController];
    if(nav){
        
    }
    [self presentViewController:navigationController animated:YES completion: nil];
    
    
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.5;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
//    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
//    [mainViewController.view.layer addAnimation:transition forKey:nil];
//
//
//    //[self.navigationController.view.layer addAnimation:transition forKey:nil];
//    //[navigationController pushViewController:mainViewController animated:YES];
    
    
    
//    CATransition *animation = [CATransition animation];
//    animation.delegate = self;
//    animation.duration = 0.7 ;  // 动画持续时间(秒)
//    animation.timingFunction = UIViewAnimationCurveEaseInOut;
//    animation.type = kCATransitionFade;//淡入淡出效果
//    
//    NSUInteger f = [[rView subviews] indexOfObject:fView];
//    NSUInteger z = [[rView subviews] indexOfObject:zView];
//    [rView exchangeSubviewAtIndex:z withSubviewAtIndex:f];
//    
//    [[rView layer] addAnimation:animation forKey:@"animation"];
//    
//    //[self performSelector:@selector(ToUpSide) withObject:nil afterDelay:2];//2秒后执行TheAnimation
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
