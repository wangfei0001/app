//
//  MemberController.m
//  Olutu
//
//  Created by fei wang on 13-7-23.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "MemberController.h"

#import "Menus.h"

#import "MyQuickMenuCell.h"

@interface MemberController (){
    NSMutableArray *menus;
}

@end

@implementation MemberController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //NSMutableArray *menu = [Menus getLeftMenu];
    menus = [[NSMutableArray alloc] initWithCapacity:3];
    
    NSArray *keys = [NSArray arrayWithObjects:@"title", @"image", @"controller", nil];
    NSArray *objects = [NSArray arrayWithObjects:@"我的收藏", @"", @"mycontroller", nil];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects
                                                           forKeys:keys];
    [menus addObject:dictionary];
    
    NSArray *keys2 = [NSArray arrayWithObjects:@"title", @"image", @"controller", nil];
    NSArray *objects2 = [NSArray arrayWithObjects:@"我的计划", @"ShowMyPlan", @"ShowMyPlan", nil];
    NSDictionary *dictionary2 = [NSDictionary dictionaryWithObjects:objects2
                                                           forKeys:keys2];
    [menus addObject:dictionary2];
    
    NSArray *keys3 = [NSArray arrayWithObjects:@"title", @"image", @"controller", nil];
    NSArray *objects3 = [NSArray arrayWithObjects:@"我的游记", @"", @"ShowMyProfile", nil];
    NSDictionary *dictionary3 = [NSDictionary dictionaryWithObjects:objects3
                                                            forKeys:keys3];
    [menus addObject:dictionary3];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellName = @"MyQuickMenuCell";     //  0
	
	MyQuickMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];      //   1
	if (cell == nil) {
		cell = [[MyQuickMenuCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: cellName];    //  2
	}
    
    cell.menuTitle.text = [[menus objectAtIndex:indexPath.row] objectForKey:@"title"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:[[menus objectAtIndex:indexPath.row] objectForKey:@"controller"] sender:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [menus count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
