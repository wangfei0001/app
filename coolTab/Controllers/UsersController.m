//
//  UsersController.m
//  coolTab
//
//  Created by fei wang on 30/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import "UsersController.h"

#import "ProfileController.h"

#import "User.h"

#import "RestClient.h"

#import "Api.h"

@interface UsersController ()

@end



@implementation UsersController
{
    
    NSDictionary *users;
    
    Api *api;
    
    RestClient *restClient;
    
    MBProgressHUD *HUD;
}

@synthesize listView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setNavBar:self.navBar];
    [super setMainView:self.listView];
    
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    [HUD show:YES];
    
    [NSThread detachNewThreadSelector:@selector(myTask) toTarget:self withObject:nil];
    
    self.title = @"我的关注";
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStylePlain target:self action:@selector(editButtonClick:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
}

- (void)myTask{



    api = [Api new];
    
    users = [api getFollowers:1];
    
    NSMutableArray *newUsers = [[NSMutableArray alloc]initWithCapacity:0];

    for(id key in users) {
        id value = [users objectForKey:key];
        
        NSDictionary *avatar = [value objectForKey:@"avatar"];

        User *user = [[User alloc]init:[value objectForKey:@"screenname"]
                                gender:true
                                avatar:[avatar objectForKey:@"w50"]];

        [newUsers addObject:user];
    }
    
    self.listView.users = newUsers;

    
    [self.listView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
    
    [HUD performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:YES];
    //[HUD removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
}



- (IBAction)editButtonClick:(id)sender {

    
    [self.listView toggleEditMode];
}


/***
 * 调用用户详细页面
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"showUserDetailSegue"]){
        
        int indexSelectedRow = [self.listView indexPathForSelectedRow].row;
        
        User *user = [self.listView.users objectAtIndex:indexSelectedRow];

        NSLog(@"select user=>%@",user);
        
        [[segue destinationViewController] setUser:user];
        
        return;
    }
}


@end
