//
//  DefaultController.h
//  Olutu
//
//  Created by fei wang on 6/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "tabbarView.h"

#import "TripsView.h"

#import "MyProfileView.h"

#import "GAITrackedViewController.h"

@class tabbarView;
//GAITrackedViewController
@interface DefaultController : UIViewController<
                        tabViewDelegate,
                        UIActionSheetDelegate,
                        UIImagePickerControllerDelegate,
                        UINavigationControllerDelegate
                        >


@property(nonatomic,strong) tabbarView *tabbar;

@property(nonatomic,strong) NSArray *arrayViewcontrollers;

@property(nonatomic,strong) NSMutableArray *tripData;

@property (strong, nonatomic) IBOutlet TripsView *tripsView;

@property (strong, nonatomic) IBOutlet MyProfileView *myProfileView;

@end
