//
//  DefaultController.m
//  Olutu
//
//  Created by fei wang on 6/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "DefaultController.h"

#import "tabbarView.h"

#import "UsersController.h"

#import "CreateTripController.h"

#import "ImagePickerController.h"

#import "ChooseImageController.h"

#import "LoginController.h"

#import "AppDelegate.h"

#import "TripDetailController.h"

#import "Trip.h"

#import "ProfileController.h"

#import "Splash.h"

#import "SplashView.h"

#define SELECTED_VIEW_CONTROLLER_TAG 98456345


@interface DefaultController ()

@end

@implementation DefaultController{

    AppDelegate *appDelegate;
    
    MBProgressHUD *HUD;
    
    SplashView *splashView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = APP_DELEGATE;
    
    [self.navigationController setNavigationBarHidden:YES];
    
    
    
    splashView = [[SplashView alloc] init];
    
    [self.view addSubview:splashView];
    
    
    

    CGFloat orginHeight = self.view.frame.size.height- 60;
//    if (iPhone5) {
//        orginHeight = self.view.frame.size.height- 60 + addHeight;
//    }
    _tabbar = [[tabbarView alloc]initWithFrame:CGRectMake(0,  orginHeight, 320, 60)];
    _tabbar.delegate = self;
    [self.view addSubview:_tabbar];
    
    _arrayViewcontrollers = [self getViewcontrollers];
    
    [self.tripsView init];
    
    
    self.tripsView.tag = SELECTED_VIEW_CONTROLLER_TAG;
    self.tripsView.frame = CGRectMake(0,0,self.view.frame.size.width, self.view.frame.size.height- 50);
    [self.view insertSubview:self.tripsView belowSubview:_tabbar];
    
    [self touchBtnAtIndex:0];
    
    [self.view bringSubviewToFront:splashView];
    
    [NSThread detachNewThreadSelector:@selector(initThread1) toTarget:self withObject:nil];
	// Do any additional setup after loading the view.

//    UITabBar *tabbar = [UITabBar appearance];
//    [tabbar setShadowImage:[UIImage imageNamed:@"transparentShadow.png"]];
//    tabbar.frame = CGRectMake(100, tabbar.frame.origin.y, tabbar.frame.size.width, tabbar.frame.size.height);
//    [tabbar setBackgroundImage:[UIImage imageNamed:@"head.jpeg"]];
//    UIView * mView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 48)];
//    [mView setBackgroundColor:[UIColor whiteColor]];
//    [mView setBackgroundColor:[[UIColor alloc] initWithRed:1.0
//                                               green:0.0
//                                                blue:0.0
//                                               alpha:0.5]];
//    [tabbar insertSubview:mView atIndex:1];
    
    
}


- (void)initThread1
{
    NSLog(@"enter init thread, loading trips");
    
    appDelegate.session.tripsData = [Trip loadTrips]; //load trip data, will be shown on default page
    
    
    
#ifdef ENABLE_SPLASH_IMAGES
    [self loadSlashImages];
    [self initThread];
#else
    //[self performSegueWithIdentifier:@"loadDefaultSegue" sender:self];
    [self initThread];
    [splashView removeFromSuperview];
    
    return;
#endif
    
    
    [UIView animateWithDuration:1 animations:^{
        splashView.alpha = 0.0;
    }
                     completion:(void (^)(BOOL)) ^{
    
                         [splashView removeFromSuperview];
                         //[self performSegueWithIdentifier:@"loadDefaultSegue" sender:self];
                         
                     }
     ];
}

/***
 * 导入图片
 */
- (void)loadSlashImages
{
    Splash *splash = [[Splash alloc] init];
    
    [splashView performSelectorOnMainThread:@selector(sendContent:)
                                   withObject:[splash load]
                                waitUntilDone:NO];
    
    while(!splashView.animationDone);
    
    if(splashView.animationDone){
        NSLog(@"done");
    }else{
        NSLog(@"not done");
    }
}

//
- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIView* currentView = [self.view viewWithTag:SELECTED_VIEW_CONTROLLER_TAG];
    currentView.frame = CGRectMake(0,0,320,460);

    
    //self.trackedViewName = @"Default Screen";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray *)getViewcontrollers
{
    NSArray* tabBarItems = nil;
    
    //UIViewController *first = [self.storyboard instantiateViewControllerWithIdentifier:@"mainController"];
    
    //SecondViewController *second = [[SecondViewController alloc]init];
    
    tabBarItems = [NSArray arrayWithObjects:
                   [NSDictionary dictionaryWithObjectsAndKeys:@"tabicon_home", @"image",@"tabicon_home", @"image_locked", nil, @"viewController",@"主页",@"title", nil]/*,
                   [NSDictionary dictionaryWithObjectsAndKeys:@"tabicon_home", @"image",@"tabicon_home", @"image_locked", second, @"viewController",@"主页",@"title", nil]*/,nil];
    return tabBarItems;
    
}

-(void)touchBtnAtIndex:(NSInteger)index
{
    if(index == 4){
        if([appDelegate.session isGuest]){
            [self performSegueWithIdentifier:@"showlogin" sender:self];
        }else{
            [self performSegueWithIdentifier:@"ShowMemberPage" sender:self];
        }
    }else{
//        UIView* currentView = [self.view viewWithTag:SELECTED_VIEW_CONTROLLER_TAG];
//        [currentView removeFromSuperview];
        
        [self.view bringSubviewToFront:self.tripsView];
        [self.view bringSubviewToFront:_tabbar];
    }
    
    if(index != 0){
        [self.tabbar reset];
    }
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 0){          //拍照
        [self showImagePicker:UIImagePickerControllerSourceTypeCamera];
    }else if(buttonIndex == 1){     //相册
        [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary];
    }else if(buttonIndex == 2){
        
    }else if(buttonIndex == 3){
  
    }else{                          //cancel

    }
    
}

- (void)showImagePicker:(int)type{
    ImagePickerController *imagePickerController = (ImagePickerController *)[self.storyboard instantiateViewControllerWithIdentifier:@"imagePickerController"];
    
    imagePickerController.sourceType = type;
    imagePickerController.delegate = self;
    
    [self performSegueWithIdentifier:@"ShowImagePicker" sender:self];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    ChooseImageController * chooseImageController = (ChooseImageController *)[self.storyboard instantiateViewControllerWithIdentifier:@"chooseImageController"];
    
    chooseImageController.delegate = self;
    
    chooseImageController.image = pickedImage;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self.navigationController pushViewController:chooseImageController animated:YES];
    
}



/***
 * 遮盖整个tripsview
 */
-(void)flyingButtonExpand:(id)sender{
//    bFlyingExpanded = !bFlyingExpanded;
//    
//    if(bFlyingExpanded){
//        [self.tripsView ShowOverlay];
//    }else{
//        
//    }
}

-(void)collapseDone:(id)sender{
//    [self.tripsView HideOverlay];
}

-(void)expandDone:(id)sender{
    NSLog(@"Not implement yet!");
}



- (void)userListClick:(id)sender
{
    UsersController * userController = (UsersController *)[self.storyboard instantiateViewControllerWithIdentifier:@"userController"];
    
    [self.navigationController pushViewController:userController animated:YES];
}




- (void)createTripClick:(id)sender{
    CreateTripController * createController = (CreateTripController *)[self.storyboard instantiateViewControllerWithIdentifier:@"createTripController"];
    
    
    [self.navigationController pushViewController:createController animated:YES];
    
}


- (void)takePhotoClick:(id)sender{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:@"上传照片"
                                  delegate:self
                                  cancelButtonTitle:@"取消"
                                  destructiveButtonTitle:@"拍照"
                                  otherButtonTitles:@"从相册中选取", @"新浪微博", @"腾讯微博", nil];
    [actionSheet showInView:self.view];
}


/***
 * 移动Main Float View to left/right
 * fold: false-向右展开, true-向左收缩
 */
-(void)moveFloatView:(id)sender animate:(BOOL)animate;
{
    
}

- (void)initThread{    
    NSDate *dat = [NSDate date];
    
    NSTimeInterval preTime=[dat timeIntervalSinceNow];
    
    //load trips
    self.tripsView.data = appDelegate.session.tripsData;
    
    [self.tripsView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
    
    NSTimeInterval curTime = [dat timeIntervalSinceNow];
    
    NSTimeInterval timeSpend = curTime - preTime;
    
    
    NSLog(@"Loading time:%f", timeSpend);
    
}


/***
 * 准备调用Segue
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"ShowTripDetail"]){
        TripDetailController *vc = [segue destinationViewController];
        
        int idx = [self.tripsView indexPathForSelectedRow].row;
        
        id data = [self.tripsView.data objectAtIndex:idx];
        NSLog(@"%@", data);
        
        vc.tripId = [[data objectForKey:@"id_trip"] intValue];
//
//        HUD = [[MBProgressHUD alloc] initWithView:self.view];
//        [self.view addSubview:HUD];
//        
//        [HUD show:YES];
//        //load trip details
//        Trip *trip = [[Trip new]initWithTripId:vc.tripId];
//        
//        vc.data = [trip loadPins:1];
//        
//        NSLog(@"Load trip pins: %@ %d", vc.data, vc.data.count);
//        
//        User *_user = [trip getUser];
//        
//        [vc.user addObject:_user];
//        
//        [HUD removeFromSuperview];
        
//        NSLog(@"prepareForSegue is called %d Image total:%d", vc.tripId, vc.data.count);
        return;
    }
}


@end
