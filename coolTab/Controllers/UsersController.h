//
//  UsersController.h
//  coolTab
//
//  Created by fei wang on 30/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseController.h"

#import "MBProgressHUD.h"

#import "UsersView.h"

#import "NavigationBar.h"

@interface UsersController : BaseController<MBProgressHUDDelegate, UISearchBarDelegate>

@property (nonatomic, retain) IBOutlet UsersView *listView;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet NavigationBar *navBar;

@end
