//
//  BigMapController.h
//  Olutu
//
//  Created by fei wang on 5/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>

#import <CoreLocation/CoreLocation.h>

@interface BigMapController : UIViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property CLLocationCoordinate2D coord;

@end
