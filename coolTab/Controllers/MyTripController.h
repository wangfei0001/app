//
//  MyTripController.h
//  Olutu
//
//  Created by fei wang on 15/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyTripController : UITabBarController<UITabBarControllerDelegate>

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
