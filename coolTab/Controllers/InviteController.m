//
//  InviteController.m
//  coolTab
//
//  Created by fei wang on 23/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "InviteController.h"

@interface InviteController ()

@end

@implementation InviteController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
