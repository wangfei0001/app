//
//  PostController.m
//  Olutu
//
//  Created by fei wang on 9/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "PostController.h"

#import "BigMapController.h"

#import "MapPin.h"

#import "MBProgressHUD.h"

#define VIEW_MODE_EDIT_DESC 1

#define VIEW_MODE_NORMAL 0

@interface PostController ()


@end

@implementation PostController{
    int mode;
    
    CLLocationCoordinate2D coord;       //当前位置
    
    MBProgressHUD *HUD;
}

@synthesize locationManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if(self.image)
        self.imageView.image = self.image;
    else
        self.imageView.image = [UIImage imageNamed:@"no-image.png"];
    
    mode = VIEW_MODE_NORMAL;
    
    [self setLocationManager:[[CLLocationManager alloc] init]];
    [locationManager setDelegate:self];
    [locationManager setDistanceFilter:kCLDistanceFilterNone];
    [locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    
    //[self initGeo];
    [NSThread detachNewThreadSelector:@selector(initGeo) toTarget:self withObject:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}

-(void)initGeo{
    [locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    
    coord.latitude = newLocation.coordinate.latitude;
    coord.longitude= newLocation.coordinate.longitude;
    MKCoordinateSpan span;
    span.latitudeDelta= 0;
    span.longitudeDelta= 360/pow(2, 16)*self.mapView.frame.size.width/256;
    MKCoordinateRegion region;
    region.center = coord;
    region.span = span;
    
    //MKCoordinateSpan span = MKCoordinateSpanMake(0, 360/pow(2, zoomLevel)*self.frame.size.width/256));
    MapPin *pin = [[MapPin alloc] initWithCoordinates:coord placeName:@"我的位置" description:@""];
    [self.mapView addAnnotation:pin];
    
    [self.mapView setRegion:region animated:NO];
    
    self.geoLabel.text = [NSString stringWithFormat:@"%.2f %.2f", newLocation.coordinate.latitude, newLocation.coordinate.longitude];
    
    [locationManager stopUpdatingLocation];
}


//-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
//    if([identifier isEqualToString:@"cancelpost"]){
//        
//
//        return NO;
//    }
//    return YES;
//}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0: //cancel
            break;
        case 1: //ok
            [self.navigationController popViewControllerAnimated:YES];
            break;
        default:
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"cancelpost"]){
        //ViewController *vc = [segue destinationViewController];

        //[self.navigationController presentViewController:vc animated:YES completion:nil];
    }
}


- (IBAction)cancelAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle: NSLocalizedString(@"确认",nil)
                          message: NSLocalizedString(@"是否放弃当前修改内容?",nil)
                          delegate: self
                          cancelButtonTitle: NSLocalizedString(@"取消",nil)
                          otherButtonTitles: NSLocalizedString(@"确定",nil), nil];
    
    [alert show];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)iconClick:(id)sender indentify:(NSString *)indentify index:(int)index selected:(BOOL)selected{
    NSLog(@"click %@ index:%d selected:%d", indentify, index, selected);
}
- (IBAction)postAction:(id)sender {
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    
    //HUD.delegate = self;
	[HUD show:YES];
    NSLog(@"Post");
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = touches.anyObject;
    
    if(touch.view == self.view){
        if(mode == VIEW_MODE_EDIT_DESC){
            [self.desciptionText resignFirstResponder];
            [self.view becomeFirstResponder];
        }
    }else if(touch.view == self.imageView){
        NSLog(@"imageview");
        
        
    }else if(touch.view == self.geoLabel){
        NSLog(@"geo");
        BigMapController * mapController = (BigMapController *)[self.storyboard instantiateViewControllerWithIdentifier:@"bigMapController"];
        mapController.coord = coord;
        
        [self.navigationController pushViewController:mapController animated:YES];
    }
    
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    mode = VIEW_MODE_EDIT_DESC;        self.imageView.hidden = YES;
    self.tripNameView.hidden = YES;
    self.mapView.hidden = YES;
    self.shareLabel.hidden = YES;
    self.shareView.hidden = YES;
    
    [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{

        CGRect newFrame = CGRectMake(self.desciptionText.frame.origin.x, self.desciptionText.frame.origin.y - 180, self.desciptionText.frame.size.width, self.desciptionText.frame.size.height);
        self.desciptionText.frame = newFrame;

    } completion:^(BOOL finished) {

    }];

}
- (void)textViewDidEndEditing:(UITextView *)textView{
    mode = VIEW_MODE_NORMAL;
    [UIView transitionWithView:self.view duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
        CGRect newFrame = CGRectMake(self.desciptionText.frame.origin.x, self.desciptionText.frame.origin.y + 180, self.desciptionText.frame.size.width, self.desciptionText.frame.size.height);
        self.desciptionText.frame = newFrame;
        
    } completion:^(BOOL finished){
        self.imageView.hidden = NO;
        self.tripNameView.hidden = NO;
        self.mapView.hidden = NO;
        self.shareLabel.hidden = NO;
        self.shareView.hidden = NO;
    }];
}


@end
