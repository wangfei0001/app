//
//  RegisterController.m
//  Olutu
//
//  Created by fei wang on 8/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "RegisterController.h"

#import "AFHTTPClient.h"

#import "AppDelegate.h"

#import "AFJSONRequestOperation.h"

@interface RegisterController (){
    AppDelegate *appDelegate;
    
    NSString *validateMessage;      //show validate message to user
}

@end

@implementation RegisterController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = @"注册用户";
    
    appDelegate = APP_DELEGATE;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)validate
{
    BOOL result = YES;
    //validate username
    if([self.username.text length] == 0){
        validateMessage = @"请输入用户名";
        [self.username becomeFirstResponder];
        result = NO;
    }else{
        //validate email
        
        
        //validate password
    
        //validate confirm-password
    
    
    }
    return result;
}


- (IBAction)RegisterClick:(id)sender {
    
    if(![self validate]){
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"出错啦"
                                                       message:validateMessage
                                                      delegate:NULL
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:NULL];
        
        [alert show];
        return;
    }
    
    
    NSURL *url = [NSURL URLWithString:@"http://olutu-yii/"];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"elinor0001", @"username",
                                @"elinor0001@hotmail.com", @"email",
//                                @"fei", @"fname",
//                                @"wang", @"lname",
                                @"616682", @"password",
                                nil];

    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"api/v1/users" parameters:parameters];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
        success:^(NSURLRequest *request , NSURLResponse *response , id json) {
    
        NSLog(@"%@", json);
            
        BOOL status = [[json valueForKey:@"status"] boolValue];
        if (status) {
//            [username resignFirstResponder];
//            [email resignFirstResponder];
//            [self.navigationController dismissModalViewControllerAnimated:NO];
            
            Config *config = appDelegate.session.config;
            config.uid = [[json valueForKey:@"id_user"] intValue];
            
        }
        else {
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
                                                           message:@"Please try again"
                                                          delegate:NULL
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:NULL];
            
            [alert show];

        }
        
    }
                                         
    failure:^(NSURLRequest *request , NSURLResponse *response , NSError *error , id JSON) {

        NSLog(@"%@", error);
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
                                message:@"There was a problem connecting to the network!"
                               delegate:NULL
                      cancelButtonTitle:@"OK"
                      otherButtonTitles:NULL];

        [alert show];
    }];
    [operation start];
}

@end
