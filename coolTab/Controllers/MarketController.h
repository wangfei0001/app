//
//  MarketController.h
//  Olutu
//
//  Created by fei wang on 13-7-26.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarketController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@end
