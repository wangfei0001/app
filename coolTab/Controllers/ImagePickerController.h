//
//  ImagePickerController.h
//  Olutu
//
//  Created by fei wang on 4/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImagePickerController : UIImagePickerController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end
