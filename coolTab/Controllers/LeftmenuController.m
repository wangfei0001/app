//
//  LeftmenuController.m
//  Olutu
//
//  Created by fei wang on 10/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "LeftmenuController.h"

#import "LeftPanelCell.h"

#import "Menus.h"

@interface LeftmenuController ()

@end

@implementation LeftmenuController{
        NSMutableArray *menuData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //init menu
    menuData = [Menus getLeftMenu];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark For Left Panel Setting Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [menuData count] + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0){
        return 1;
    }else
        return [[[menuData objectAtIndex:section - 1] objectAtIndex:1] count];
}

//設定分類開頭標題
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(section > 0){
        return [[menuData objectAtIndex:section - 1] objectAtIndex:0];
    }else{
        return @"";
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0){
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"leftpanelavatar" owner:self options:nil];
        
        return [subviewArray objectAtIndex:0];
    }else{
        LeftPanelCell *cell = (LeftPanelCell *)[tableView dequeueReusableCellWithIdentifier:@"leftPanelCell"];
        
        id menu = [[[menuData objectAtIndex:indexPath.section - 1] objectAtIndex:1] objectAtIndex:indexPath.row];
        NSLog(@"%@", menu);
        cell.mainView.text.text = [menu objectForKey:@"name"];
        
        cell.backgroundColor = [UIColor whiteColor];
        
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == 0) return 48;
    else return 45;
}

//customize section header;
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        return nil;
    }else{
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 24)];
        
        CGFloat nRed=128.0/255.0;
        CGFloat nBlue=98.0/255.0;
        CGFloat nGreen=53.0/255.0;
        UIColor *myColor=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, 24)];
        
        label.text = [[menuData objectAtIndex:section - 1]objectAtIndex:0];
        [label setFont:[UIFont fontWithName:@"宋体" size:10]];
        label.textColor = [UIColor whiteColor];
        
        label.backgroundColor = [UIColor clearColor];
        
        headerView.backgroundColor = myColor;
        
        [headerView addSubview:label];
        return headerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0) return 0;
    else return 30;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *controller = nil;
    NSLog(@"section:%d index:%d", indexPath.section, indexPath.row);
    
    if(indexPath.section > 0){
        id menu = [[[menuData objectAtIndex:indexPath.section - 1] objectAtIndex:1] objectAtIndex:indexPath.row];
        
        NSString *controllerName = [menu objectForKey:@"controller"];
        
        if(controllerName){
            controller = [self.storyboard instantiateViewControllerWithIdentifier:controllerName];
            [UIView beginAnimations:@"View Flip" context:nil];
            [UIView setAnimationDuration:0.4];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
            
            [UIView setAnimationTransition:
             UIViewAnimationTransitionFlipFromRight
                                   forView:self.navigationController.view cache:NO];
            
            
            [self.navigationController pushViewController:controller animated:YES];
            [UIView commitAnimations];
            //[self.navigationController pushViewController:controller animated:YES];
        }
    }
}

@end
