//
//  BaseController.m
//  Olutu
//
//  Created by fei wang on 7/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "BaseController.h"

@interface BaseController ()

@end

@implementation BaseController{
    /* Bar 是否隐藏 */
    BOOL barHidden;
    
    /* 计算偏移量 */
    float scrollOffset;
    
    UINavigationBar *navBar;
    
    UIView *mainView;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
        scrollOffset = 0;
        barHidden = NO;
        navBar = nil;
        mainView = nil;
    }
    return self;
}

- (void)setNavBar:(UINavigationBar *)bar{
    navBar = bar;
}

- (void)setMainView:(UIView *)view{
    mainView = view;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    float y = scrollView.contentOffset.y;
    
    scrollOffset = y;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    return;
    float y = scrollView.contentOffset.y;
    
    if(y < scrollOffset - 10){
        if(barHidden){
            NSLog(@"Up  %f   %f", y, scrollOffset);

            [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionTransitionNone animations:^{
                if(navBar){
                    navBar.frame = CGRectMake(0,
                                              0,
                                              navBar.frame.size.width,
                                              navBar.frame.size.height);
                    
                    if(mainView) mainView.frame = CGRectMake(0, navBar.frame.size.height, self.view.frame.size.width,self.view.frame.size.height - navBar.frame.size.height);
                }else{
                    NSLog(@"Not set Nav");
                }
            } completion:^(BOOL finished) {
            }];
            
            barHidden = NO;
        }
    }else if(y > scrollOffset + 10){
        if(!barHidden){
            NSLog(@"Down   %f    %f", y, scrollOffset);
            
            [UIView transitionWithView:self.view duration:0.5 options:UIViewAnimationOptionTransitionNone animations:^{
                if(navBar){
                    navBar.frame = CGRectMake(
                                              0,
                                              -navBar.frame.size.height,
                                              navBar.frame.size.width,
                                              navBar.frame.size.height);
                    if(mainView) mainView.frame = self.view.frame;
                }else{
                    NSLog(@"Not set Nav");
                }
            } completion:^(BOOL finished) {
            }];
            
            
            barHidden = YES;
        }
    }
    
    
}


@end
