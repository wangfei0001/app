//
//  LoginController.m
//  Olutu
//
//  Created by fei wang on 8/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "LoginController.h"

#import "Global.h"

#import "AppDelegate.h"

#import "FacebookClient.h"

#import "AFHTTPClient.h"

#import "AFJSONRequestOperation.h"


@interface LoginController ()

@end

@implementation LoginController{
    SinaWeibo *sinaWeiBo;
    
    AppDelegate *appDelegate;
    
    NSString *validateMessage;      //show validate message to user
}

//- (SinaWeibo *)sinaweibo
//{
//    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    return delegate.sinaweibo;
//}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


/***
 * Validate the form
 */
- (BOOL)validate
{
    BOOL result = YES;
    //validate username
    if([self.username.text length] == 0){
        validateMessage = @"请输入用户名";
        [self.username becomeFirstResponder];
        result = NO;
    }else{
        //validate password
        if([self.password.text length] == 0){
            validateMessage = @"请输入用户密码";
            [self.password becomeFirstResponder];
            result = NO;
        }
        
        
    }
    return result;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    appDelegate = APP_DELEGATE;
    
    sinaWeiBo = [[SinaWeibo alloc] initWithAppKey:kAppKey appSecret:kAppSecret appRedirectURI:kAppRedirectURI andDelegate:self];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
    if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
    {
        sinaWeiBo.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
        sinaWeiBo.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
        sinaWeiBo.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
    }
    self.title = @"登录";
    self.password.secureTextEntry = YES;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)weiboLoginAction:(id)sender {
    NSLog(@"weibo login");
    //SinaWeibo *sinaweibo = [self sinaweibo];
    
    [sinaWeiBo logIn];
}
- (IBAction)facebookLoginClick:(id)sender {
    
    [appDelegate.facebookClient login];
    
    appDelegate.facebookClient.delegate = self;
    
}
- (IBAction)loginClick:(id)sender {
    if(![self validate]){
        UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"出错啦"
                                                       message:validateMessage
                                                      delegate:NULL
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:NULL];
        
        [alert show];
        return;
    }
    
    NSURL *url = [NSURL URLWithString:@"http://olutu-yii/"];
    
//    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
//                                @"elinor0001", @"username",
//                                @"elinor0001@hotmail.com", @"email",
//                                //                                @"fei", @"fname",
//                                //                                @"wang", @"lname",
//                                @"616682", @"password",
//                                nil];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    [httpClient clearAuthorizationHeader];
    [httpClient setAuthorizationHeaderWithUsername:self.username.text password:self.password.text];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"POST" path:@"api/v1/login" parameters:nil];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
        success:^(NSURLRequest *request , NSURLResponse *response , id json) {
            
            NSLog(@"%@", json);
            
            BOOL status = [[json valueForKey:@"status"] boolValue];
            if (status) {
                //            [username resignFirstResponder];
                //            [email resignFirstResponder];
                //            [self.navigationController dismissModalViewControllerAnimated:NO];
                
                Config *config = appDelegate.session.config;
                config.uid = [[json valueForKey:@"id_user"] intValue];
                
            }
            else {
                UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
                                                               message:@"Please try again"
                                                              delegate:NULL
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:NULL];
                
                [alert show];
                
            }
            
        }

        failure:^(NSURLRequest *request , NSURLResponse *response , NSError *error , id JSON) {
            
            NSLog(@"%@", error);
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
                                                           message:@"There was a problem connecting to the network!"
                                                          delegate:NULL
                                                 cancelButtonTitle:@"OK"
                                                 otherButtonTitles:NULL];
            
            [alert show];
        }];
    [operation start];
}

- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo
{
    NSLog(@"sinaweiboDidLogIn userID = %@ accesstoken = %@ expirationDate = %@ refresh_token = %@", sinaweibo.userID, sinaweibo.accessToken, sinaweibo.expirationDate,sinaweibo.refreshToken);
    
//    [self resetButtons];
//    [self storeAuthData];
}

- (void)authorisedDone:(BOOL)success
{
    if(success){
        NSLog(@"Auth Success");
        
        
        
//        if([[Session instance] isGuest]){  //have registered, need to call api
//            
//            UserApi *userApi = [[UserApi alloc] init];
//            
//            Config *config = [Session instance].config;
//            [userApi login_fb:config.facebook_id success:^(NSString *message, id JSON) {
//                NSLog(@"Success");
//                NSLog(@"%@",JSON);
//                id user = [JSON objectForKey:@"user"];
//                Config *config = [Session instance].config;
//                
//                BOOL overwrite = config.uid == 0? NO:YES;
//                
//                config.uid = [[user objectForKey:@"id"] intValue];
//                
//                [[Session instance] save:overwrite];
//                [appDelegate.rootNav popViewControllerAnimated:YES];
//                
//            } failure:^(NSString *message, int code) {
//                NSLog(@"Failure->%@", message);
//            }];
//        }else{
//            [appDelegate.rootNav popViewControllerAnimated:YES];
//        }
        
        
        
        
    }else{
        NSLog(@"Auth Failed");
    }
}

@end
