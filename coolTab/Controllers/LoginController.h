//
//  LoginController.h
//  Olutu
//
//  Created by fei wang on 8/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FacebookClient.h"

#import "SinaWeibo.h"

#import "SinaWeiboRequest.h"

@interface LoginController : UIViewController<SinaWeiboDelegate, SinaWeiboRequestDelegate, FacebookClientDelegate>

@property (weak, nonatomic) IBOutlet UITextField *username;

@property (weak, nonatomic) IBOutlet UITextField *password;

@end
