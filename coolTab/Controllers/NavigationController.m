//
//  NavigationController.m
//  Olutu
//
//  Created by fei wang on 7/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController ()

@end

@implementation NavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        //        CGFloat navBarHeight = 35.0f;
        //        CGRect frame = self.navigationController.navigationBar.frame;
        //        frame.size.height = navBarHeight;
        //        [self.navigationController.navigationBar setFrame:frame];
        
        //        UIImage *image = [UIImage imageNamed:@"head.jpeg"];
        //        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        UIColor *color = [UIColor colorWithRed:139.0/255 green:178.0/255 blue:38.0/255 alpha:1];
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        CGContextSetFillColor(context, CGColorGetComponents( [color CGColor]));
        CGContextFillRect(context, self.navigationBar.frame);
        [self.navigationBar setBarStyle:UIBarStyleBlack];
        [self.navigationBar setTintColor:color];
        
        // [[UIBarButtonItem appearance] setBackButtonBackgroundImage:image forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
        
    }
    self.delegate = self;
}

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if ( viewController == self.viewControllers[0] ) {
        [navigationController setNavigationBarHidden:YES animated:animated];
    } else if ( [navigationController isNavigationBarHidden] ) {
        [navigationController setNavigationBarHidden:NO animated:animated];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
