//
//  TripDetailController.m
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "TripDetailController.h"

#import "ProfileViewCell.h"

#import "MapViewCell.h"

#import "ProfileController.h"

#import "ImageController.h"

#import "Trip.h"

#import "UIImageView+WebCache.h"

#import "Trip.h"

#import "PullTableFooter.h"

#import "AppDelegate.h"

#import "Api.h"

#import <MapKit/MapKit.h>

#import <CoreLocation/CoreLocation.h>

@interface TripDetailController ()


@end

@implementation TripDetailController{
    
    int selectedIndex;
    
    ProfileViewCell *profileCell;

}

@synthesize tripId;

@synthesize user;

@synthesize data;



- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
        //self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [super setNavBar:self.navBar];
    [super setMainView:self.detailTable];
    
    self.detailTable.showsVerticalScrollIndicator = NO;
    
    //static NSString *cellIdentifier = @"tripdetailfooter";
    
    PullTableFooter *cell = [[PullTableFooter alloc]initWithFrame:CGRectMake(0, 0, 320, 36)];
    self.detailTable.tableFooterView = cell;
//    self.detailTable.layer.borderColor = [UIColor clearColor].CGColor;
    
    /*Remove border for footer view */
//    UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
//    backView.backgroundColor = [UIColor clearColor];
//    cell.backgroundView = backView;
    
    
    self.profileBackgroundImage.image = [UIImage imageNamed:@"demo.jpg"];
    self.profileBackgroundImage.frame = CGRectMake(0,-44, 360, 240);
    
    
//    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
//                                @"1", @"is_plan",
//                                nil];
    
    [Api call: @"trips/1/tripPost"
       method: @"GET"
        param: nil
      success:^(NSURLRequest *request , NSURLResponse *response , id json) {
          
          NSLog(@"%@", json);
          
          BOOL status = [[json valueForKey:@"status"] boolValue];
          if (status) {
              self.data = [json valueForKey:@"data"];
              [self.detailTable reloadData];
          }
          else {
              //            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
              //                                                           message:@"Please try again"
              //                                                          delegate:NULL
              //                                                 cancelButtonTitle:@"OK"
              //                                                 otherButtonTitles:NULL];
              //
              //            [alert show];
              
          }
          
      }];
    
    
    
}


- (void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    
    [AppDelegate showTabBar:self.tabBarController];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)avatarClick:(id)sender{

    ProfileController *profileController = (ProfileController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ProfileController"];

    [self.navigationController pushViewController:profileController animated:YES];
}


- (void)imageClick:(id)sender indexPath:(NSIndexPath *)indexPath{
    
    ImageController *imageController = (ImageController *)[self.storyboard instantiateViewControllerWithIdentifier:@"ImageController"];
    
    imageController.data = self.data;
    
    imageController.selectedIndex = indexPath.row - 2;
    
    
    
    [self.navigationController pushViewController:imageController animated:YES];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.data count] + 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 120;
    }else if(indexPath.row == 1){
        return 136;
    }else
        return 180 + 30;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [super scrollViewDidScroll:scrollView];
    
    CGRect oriFrame = self.profileBackgroundImage.frame;
    CGPoint offset = scrollView.contentOffset;
    
    if(offset.y < 0){
        NSLog(@"1");
        if(offset.y <= -120){
            offset.y = -120;
            
        }else if(offset.y > -120 && offset.y <= -44){     //下拉到一定程度
        }else{
            NSLog(@"3");
            [self.profileBackgroundImage setFrame:CGRectMake(0,-44 - offset.y,oriFrame.size.width,oriFrame.size.height)];
        }
        
        scrollView.contentOffset = offset;
        
    }else{
        NSLog(@"2");
        [self.profileBackgroundImage setFrame:CGRectMake(0,-44 - offset.y,oriFrame.size.width,oriFrame.size.height)];
    }
    NSLog(@"top*** %f  %f->%f", scrollView.contentOffset.y, self.profileBackgroundImage.frame.origin.y, self.profileBackgroundImage.frame.size.height);
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(scrollView.contentOffset.y > ((scrollView.contentSize.height - scrollView.frame.size.height))){
    
        
        [NSThread detachNewThreadSelector:@selector(loadMoreData) toTarget:self withObject:nil];
    }
}

- (void)loadMoreData{
    
    PullTableFooter *footer = (PullTableFooter *)self.detailTable.tableFooterView;
    [footer startLoading:true];
    
    Trip *trip = [[Trip new]initWithTripId:self.tripId];
    
    NSMutableArray *newData = [trip loadPins:1];
    
    if(newData){
        for(int i = 0; i < newData.count; i++){
            [self.data addObject:[newData objectAtIndex:i]];
        }
        [self.detailTable performSelectorOnMainThread:@selector(reloadData)
                               withObject:nil
                            waitUntilDone:NO];
    }
    [footer startLoading:false];
}


//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
//    CGPoint tappedPt = [[touches anyObject] locationInView: self.view];
//    int     xPos = tappedPt.x;
//    int     yPos = tappedPt.y;
//    NSLog(@"%d %d", xPos, yPos);
//    //NSLog(@"Back %f %f", self.backgroundView.frame.origin.x, self.backgroundView.frame.origin.y);
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Row:%d", indexPath.row);
    if(indexPath.row > 1){
        
        PinViewCell *cell = (PinViewCell *)[tableView dequeueReusableCellWithIdentifier:@"pinCell"];
        cell.delegate = self;
        
        //loading indicator
        UIActivityIndicatorView *loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        loadingView.center = cell.center;
        [loadingView startAnimating];
        [cell addSubview:loadingView];
        
        
        id trip = [self.data objectAtIndex:indexPath.row - 2];
        
//        NSString *urlStr = [trip objectForKey:@"thumb"];
//        if(urlStr){
//            NSURL *imgURL=[[NSURL alloc]initWithString:[trip objectForKey:@"thumb"]];
//            
//            //Here we use the new provided setImageWithURL: method to load the web image
//            
////            [cell.pinImage setImageWithURL:imgURL];
////            [loadingView removeFromSuperview];
//            
//            
//            SDWebImageManager *manager = [SDWebImageManager sharedManager];
//            [manager downloadWithURL:imgURL options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
//
//                [UIView transitionWithView:cell.pinImage
//                                  duration:0.5
//                                   options:UIViewAnimationOptionTransitionCrossDissolve
//                                animations:^{
//                                    [loadingView removeFromSuperview];
//                                    
//                                    if(image.size.height > cell.pinImage.frame.size.height ||
//                                       image.size.width > cell.pinImage.frame.size.width){
//                                    
//                                        [cell.pinImage setImage:[UIImage imageWithCGImage:CGImageCreateWithImageInRect([image CGImage], cell.pinImage.frame)]];
//                                    }else{
//                                        [cell.pinImage setImage:image];
//                                    }
//
//                                    //NSLog(@"Reset Image=>%f %f", cell.pinImage.frame.origin.x, cell.pinImage.frame.origin.y);
//                                } completion:^(BOOL finished){
//                                    //
//                                    //[cell initToolbar];
//                                   
//                                }];
//                
//                
//
//            }];

            cell.timeLabel.text = [trip objectForKey:@"content"];
            cell.detailTextLabel.text = [trip objectForKey:@"desc"];
            [cell.pinImage setImageWithURL:[NSURL URLWithString:[trip objectForKey:@"image"]]
                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
//        }else{
//            NSLog(@"What the fuck?");
//        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
        
    }else if(indexPath.row == 1){
        MapViewCell *cell = (MapViewCell *)[tableView dequeueReusableCellWithIdentifier:@"mapViewCell"];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        CLLocationCoordinate2D coord;
        coord.latitude = -33.8820553;
        coord.longitude= 151.2037314;
        MKCoordinateSpan span;
        span.latitudeDelta= 0.1;
        span.longitudeDelta= 0.1;
        MKCoordinateRegion region;
        region.center = coord;
        region.span = span;
        
        [cell.mapView setRegion:region animated:YES];
        
        return cell;
    }else{
        profileCell = [[ProfileViewCell alloc]init];
        profileCell.delegate = self;
        
        //[cell.profileView.userNameLabel setText:[self.user ]];
        [profileCell initAvatarImage];
        
        return profileCell;
    }
}

@end
