//
//  PlanController.h
//  Olutu
//
//  Created by fei wang on 15/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PlanCell.h"

@interface PlanController : UIViewController<UITableViewDataSource, UITableViewDelegate, planCellViewDelegate>


-(void)imageClick:(id)sender indexPath:(NSIndexPath *)indexPath;

@property (strong, nonatomic) IBOutlet UITableView *planView;

@end
