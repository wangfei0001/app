//
//  ViewController.h
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BaseController.h"

#import "UIImageView+WebCache.h"

#import "TripsView.h"

#import "LeftPanelView.h"

#import "MBProgressHUD.h"

@interface ViewController : UIViewController<MBProgressHUDDelegate,UIGestureRecognizerDelegate>{
    MBProgressHUD *HUD;
}

@property (retain, nonatomic) IBOutlet UIView *floatView;

@property (retain, nonatomic) IBOutlet TripsView *tripsView;

@property (retain, nonatomic) NSMutableArray *tripData;

@property (retain, nonatomic) UIImage *choosedImage;

@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
