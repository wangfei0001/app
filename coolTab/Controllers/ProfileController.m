//
//  ProfileController.m
//  coolTab
//
//  Created by fei wang on 8/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ProfileController.h"

#import "TripDetailController.h"

#import "MBProgressHUD.h"

#import "Trip.h"

#import "ProfileViewCell.h"

#import "MyTripCell.h"

#import "MyTripController.h"

#import "Api.h"

#import "AppDelegate.h"

#import "UIImageView+WebCache.h"

@interface ProfileController ()

@end

@implementation ProfileController{
    NSMutableArray *trips; //trips created by this user;
    
    AppDelegate *appDelegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = APP_DELEGATE;
    
    trips = [[NSMutableArray alloc]initWithCapacity: 0];
    
	// Do any additional setup after loading the view.
//    NSArray *segmentTextContent = [NSArray arrayWithObjects: @"First",@"Second",@"Third",@"Forth", nil];
//    [self.segment initWithItems:segmentTextContent];
//    [self.segment setFrame:CGRectMake(0, 0, 500, 30)];
//    [self.segment setSegmentedControlStyle:UISegmentedControlStyleBar];
//    [self.segment setTintColor:[UIColor darkGrayColor]];
    
//    id profileview = [self.profileView initWithFrame:self.view.bounds];
//    if(profileview){
//        
//    }
    [super setNavBar:self.navBar];
    
    [super setMainView:self.profileView];
    

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(Add:)];
    
    

    
    //[appDelegate ShowLoading:self.navigationController.view];
    
    
    //load...
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"0", @"is_plan",
                                nil];
    
    [Api call: @"users/18/myTrip"
       method: @"GET"
        param: parameters
      success:^(NSURLRequest *request , NSURLResponse *response , id json) {
          
          NSLog(@"%@", json);
          
          BOOL status = [[json valueForKey:@"status"] boolValue];
          if (status) {
              for(id plan in [json valueForKey:@"data"]){
                  NSLog(@"%@",plan);
                  NSArray *keys = [NSArray arrayWithObjects:@"title", @"image", @"controller", nil];
                  NSArray *objects = [NSArray arrayWithObjects:[plan valueForKey:@"title"], [plan valueForKey:@"image"], @"mycontroller", nil];
                  NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects
                                                                         forKeys:keys];
                  [trips addObject:dictionary];
              }
              [self.profileView reloadData];
          }
          else {
              //            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
              //                                                           message:@"Please try again"
              //                                                          delegate:NULL
              //                                                 cancelButtonTitle:@"OK"
              //                                                 otherButtonTitles:NULL];
              //
              //            [alert show];
              
          }
          
      }];
    
    //[NSThread detachNewThreadSelector:@selector(myTask) toTarget:self withObject:nil];
    self.profileBackgroundImage.image = [UIImage imageNamed:@"demo.jpg"];
}

- (void)Add: (id)sender
{
    [self performSegueWithIdentifier:@"ShowCreateTrip" sender:self];
}

//- (void)myTask{
//    
//    
////    Api *api = [[Api alloc]init];
////    NSDictionary *result = [api getTripByUser:1 page:1];
////    if(result){
////        trips = [[NSMutableArray alloc]initWithCapacity:result.count];
////        for(id obj in result){
////            [trips addObject:obj];
////        }
////    }
//    self.profileBackgroundImage.image = [UIImage imageNamed:@"demo.jpg"];
//    
//    [self.profileView performSelectorOnMainThread:@selector(reloadData)
//                                     withObject:nil
//                                  waitUntilDone:NO];
//    
//    [HUD performSelectorOnMainThread:@selector(removeFromSuperview) withObject:nil waitUntilDone:YES];
//}

- (void)viewWillAppear:(BOOL)animated{
    
}

//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    int a;
//    //    if ([[segue identifier] isEqualToString:@"goBackToTableView"])  {
//        [[segue destinationViewController] set{whatEverProperyYouWantTo}: {valueOfPropertyToSet}];
////    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"showtripdetail"]){
        
        TripDetailController *vc = [segue destinationViewController];
        
        vc.tripId = 1;
        

        //load trip details
        Trip *trip = [[Trip new]initWithTripId:vc.tripId];
        
//        vc.data = [trip loadPins:1];
//        
//        NSLog(@"Load trip pins: %@ %d", vc.data, vc.data.count);
//        
//        self.user = [trip getUser];
//        
//        [vc.user addObject:self.user];
        

    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){//profile cell
        ProfileViewCell *cell = [[ProfileViewCell alloc]init];
        
        [cell.userNameLabel setText:self.user.screenName];
        [cell initAvatarImage];
        
        return cell;
    }else{
        MyTripCell *cell = [[MyTripCell alloc] init];
        
        id obj = [trips objectAtIndex:indexPath.row - 1];
        
        cell.tripNameLabel.text = [obj objectForKey:@"title"];
        
        [cell.imageView setImageWithURL:[NSURL URLWithString:[obj objectForKey:@"image"]]
                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        return cell;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 120;
    }else
        return 100  + 10;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return trips.count + 1;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row > 0){
//        MyTripController *controller = (MyTripController *)[self.storyboard instantiateViewControllerWithIdentifier:@"myTripController"];
//        
//        [self.navigationController pushViewController:controller animated:YES];
        
        TripDetailController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tripDetailController"];
        
        vc.tripId = 1;
        

        //load trip details
        Trip *trip = [[Trip new]initWithTripId:vc.tripId];
        
//        vc.data = [trip loadPins:1];
//        
//        NSLog(@"Load trip pins: %@ %d", vc.data, vc.data.count);
//        
//        self.user = [trip getUser];
//        
//        [vc.user addObject:self.user];
        
        
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [super scrollViewDidScroll:scrollView];
    
    CGRect oriFrame = self.profileBackgroundImage.frame;
    CGPoint offset = scrollView.contentOffset;
    
    if(offset.y < 0){
        
        if(offset.y <= -120){
            offset.y = -120;
            
        }else if(offset.y > -120 && offset.y <= -60){     //下拉到一定程度
            //[self.profileBackgroundImage setFrame:CGRectMake(0,24,oriFrame.size.width,oriFrame.size.height)];
        }else{
            [self.profileBackgroundImage setFrame:CGRectMake(0,-24 - offset.y,oriFrame.size.width,oriFrame.size.height)];
        }
        
        scrollView.contentOffset = offset;
        
    }else{
        [self.profileBackgroundImage setFrame:CGRectMake(0,-24 - offset.y,oriFrame.size.width,oriFrame.size.height)];
    }
    NSLog(@"top*** %f  %f->%f", scrollView.contentOffset.y, self.profileBackgroundImage.frame.origin.y, self.profileBackgroundImage.frame.size.height);
}

@end
