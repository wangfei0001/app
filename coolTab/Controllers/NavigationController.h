//
//  NavigationController.h
//  Olutu
//
//  Created by fei wang on 7/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationController : UINavigationController<UINavigationControllerDelegate>

@end
