//
//  TripDetailController.h
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PinViewCell.h"

#import "NavigationBar.h"

#import "BaseController.h"

#import "ProfileViewCell.h"


@interface TripDetailController : BaseController<UITableViewDelegate, UITableViewDataSource, pinViewDelegate, userProfileViewDelegate>{
 
}

@property (nonatomic) int tripId;

@property (nonatomic, retain) id data;

@property (nonatomic, retain) NSMutableArray *user;

@property (weak, nonatomic) IBOutlet UITableView *detailTable;

@property (weak, nonatomic) IBOutlet NavigationBar *navBar;

@property (weak, nonatomic) IBOutlet UIImageView *profileBackgroundImage;


@end
