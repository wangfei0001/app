//
//  MainController.h
//  Olutu
//
//  Created by fei wang on 10/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainController : UIViewController

- (void)moveFloatView:(BOOL)animate;

- (BOOL)getSlideStatus;

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (strong, nonatomic) IBOutlet UIView *leftMenuView;

@end
