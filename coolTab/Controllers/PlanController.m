//
//  PlanController.m
//  Olutu
//
//  Created by fei wang on 15/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "PlanController.h"

#import "XMLReader.h"

#import "PlanTypeCell.h"

#import "PlanCell.h"

#import "Api.h"

#import "UIImageView+WebCache.h"

@interface PlanController ()

@end

@implementation PlanController{
    NSMutableArray *planData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    planData = [[NSMutableArray alloc] initWithCapacity:0];
    
    // Add button, 添加计划
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(AddPlan:)];
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                @"1", @"is_plan",
                                nil];

    [Api call: @"users/18/myTrip"
       method: @"GET" 
        param: parameters
      success:^(NSURLRequest *request , NSURLResponse *response , id json) {
          
          NSLog(@"%@", json);
          
          BOOL status = [[json valueForKey:@"status"] boolValue];
          if (status) {
              for(id plan in [json valueForKey:@"data"]){
                  NSLog(@"%@",plan);
                  NSArray *keys = [NSArray arrayWithObjects:@"title", @"image", @"controller", nil];
                  NSArray *objects = [NSArray arrayWithObjects:[plan valueForKey:@"title"], [plan valueForKey:@"image"], @"mycontroller", nil];
                  NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:objects
                                                                         forKeys:keys];
                  [planData addObject:dictionary];
              }
              [self.planView reloadData];
          }
          else {
//            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
//                                                           message:@"Please try again"
//                                                          delegate:NULL
//                                                 cancelButtonTitle:@"OK"
//                                                 otherButtonTitles:NULL];
//
//            [alert show];
              
          }
          
      }];
    
    
//    NSError *errorPointer = nil;
//    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:@"plantype" ofType:@"xml"];
//    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
//    
//    NSDictionary *dic = [XMLReader dictionaryForXMLData:xmlData error:&errorPointer];
//    
//    id root = [dic objectForKey:@"root"];
//    id plantype = [root objectForKey:@"plantype"];
//    for(id val in plantype){
//        NSLog(@"%@", val);
//        
//        [planData addObject:val];
//    }
    
}


- (void)AddPlan: (id)sender
{
    NSLog(@"创建计划");
    [self performSegueWithIdentifier:@"ShowCreatePlan" sender:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    

}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellName = @"PlanCell";     //  0
	
	PlanCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];      //   1
	if (cell == nil) {
		cell = [[PlanCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: cellName];    //  2
	}
    cell.delegate = self;
    
    //cell.imageView
//    id plan = [planData objectAtIndex:indexPath.row];
//    cell.imageView.image = [UIImage imageNamed:@"head.jpeg"];
//    cell.titleLabel.text = [plan objectForKey:@"name"];
//    cell.descLabel.text = [plan objectForKey:@"desc"];
    //int count = [plan objectForKey:@"count"];
    
    id planObj = [planData objectAtIndex:indexPath.row];
    
    cell.destination.text = [planObj objectForKey:@"title"];
    
    [cell.DisImage setImageWithURL:[NSURL URLWithString:[planObj objectForKey:@"image"]]
                   placeholderImage:[UIImage imageNamed:@"placeholder.png"]];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return planData.count;
}

-(void)imageClick:(id)sender indexPath:(NSIndexPath *)indexPath;
{
    NSLog(@"%d", indexPath.row);
    //ShowMarketView
    [self performSegueWithIdentifier:@"ShowMarketView" sender:self];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 1;
//}
@end
