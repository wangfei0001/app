//
//  ChooseImageController.h
//  Olutu
//
//  Created by fei wang on 6/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChooseImageController : UIViewController

@property id delegate;

@property (retain, nonatomic) UIImage *image;

- (IBAction)leftRotate:(id)sender;

- (IBAction)rightRotate:(id)sender;

- (IBAction)submitAction:(id)sender;

@end
