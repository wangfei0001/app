//
//  BigMapController.m
//  Olutu
//
//  Created by fei wang on 5/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "BigMapController.h"

#import "MapPin.h"

#import <CoreLocation/CoreLocation.h>

@interface BigMapController ()

@end

@implementation BigMapController{    

}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
//
//        
//        NSString *path = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"html" inDirectory:@"web"];
//        
//        NSURL *base = [NSURL fileURLWithPath:path isDirectory:NO];
//        
//        [mapView loadRequest:[NSURLRequest requestWithURL:base]];
//        
//        //[self.view addSubview:mapView];
        


    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    NSString *urlAddress = @"http://www.google.com";
//    
//    //Create a URL object.
//    NSURL *url = [NSURL URLWithString:urlAddress];
//    
//    //URL Requst Object
//    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//    
//    //Load the request in the UIWebView.
//    [mapView loadRequest:requestObj];

    
//    NSString *path = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"html" inDirectory:@"web"];
//
//    NSURL *base = [NSURL fileURLWithPath:path isDirectory:NO];
//
//    [mapView loadRequest:[NSURLRequest requestWithURL:base]];
    
    CLLocationCoordinate2D coord;
    
    if(self.coord.latitude && self.coord.longitude){
        coord = self.coord;
    }else{
        coord.latitude = -33.8820553;
        coord.longitude= 151.2037314;
    }
    
    
    MKCoordinateSpan span;
    span.latitudeDelta= 0.1;
    span.longitudeDelta= 0.1;
    MKCoordinateRegion region;
    region.center = coord;
    region.span = span;
    
    
    MapPin *pin = [[MapPin alloc] initWithCoordinates:coord placeName:@"我的位置" description:@""];
    [self.mapView addAnnotation:pin];
    
    [self.mapView setRegion:region animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
