//
//  ImageController.m
//  coolTab
//
//  Created by fei wang on 25/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ImageController.h"

#import "UIImageView+WebCache.h"

#import "AjaxLoadingView.h"


@interface ImageController ()

@end

@implementation ImageController{
    BOOL barHidden;
    
    BOOL commentMode;
    
    __weak IBOutlet UITextField *commentText;
    __weak IBOutlet UIToolbar *commentBar;
    
}

@synthesize data;

@synthesize selectedIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        commentMode = NO;
        barHidden = YES;
        
        //commentText.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    
    
    [self loadImage];

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    commentMode = NO;
    [self showComment:NO];
    return YES;
}

//-(void)textFieldDidEndEditing:(UITextField *)textField
//{
//    [self.view endEditing:YES];
//}

- (void)loadImage{
    AjaxLoadingView *ajaxLoading = [[AjaxLoadingView alloc]initWithFrame:self.image.frame isBlack:YES];
    
    [self.image addSubview:ajaxLoading];
    
    
    id trip = [data objectAtIndex:selectedIndex];
    
    NSURL *imgURL=[[NSURL alloc]initWithString:[trip objectForKey:@"thumb"]];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadWithURL:imgURL options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
        
        //                cell.pinImage.image = [UIImage imageWithCGImage:CGImageCreateWithImageInRect([image CGImage], cell.pinImage.frame)];
        
        
        [UIView transitionWithView:self.image
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [ajaxLoading removeFromSuperview];
                            [self.image setImage:image];
                            
                            NSString *intString = [NSString stringWithFormat:@"%d / %d", selectedIndex + 1, self.data.count];
                            [self.navBar.topItem setTitle:intString];
                            
                        } completion:^(BOOL finished){
                            
                        }];
        
        
        
    }];
}

- (void)viewDidDisappear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
//    [self.navigationController popViewControllerAnimated:YES];
//    
//    return;
//    UITouch *touch = [touches anyObject];
//    NSUInteger tapCount = [touch tapCount];
//    CGPoint location = [touch locationInView:touch.view];
//    
//    NSLog(@"location-%f,%f", location.x,location.y);
//}



- (IBAction)handleLeftSwipe:(UISwipeGestureRecognizer *)sender {
    if(selectedIndex > 0){
        selectedIndex--;
        [self loadImage];
    }
    NSLog(@"Left Current:%d %d", selectedIndex, data.count);
}

- (IBAction)handleRightSwipe:(UISwipeGestureRecognizer *)sender {
    if(selectedIndex < data.count - 1){
        selectedIndex++;
        [self loadImage];
    }
    NSLog(@"Right Current:%d %d", selectedIndex, data.count);
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void)showComment:(BOOL)show{
    CGRect oriRect = CGRectMake(0,
                                self.view.frame.size.height,
                                commentBar.frame.size.width,
                                commentBar.frame.size.height);
    
    CGRect desRect = CGRectMake(0, 220, commentBar.frame.size.width, commentBar.frame.size.height);
    
    if(show){
        commentBar.frame = oriRect;
        [UIView transitionWithView:commentBar
                          duration:0.5
                           options:UIViewAnimationTransitionNone
                        animations:^{
                            
                            commentBar.hidden = NO;
                            commentBar.frame = desRect;
                            
                        } completion:^(BOOL finished){
                            
                        }];
        commentBar.hidden = NO;
        [self resignFirstResponder];
        [commentText becomeFirstResponder];
    }else{
        commentBar.frame = desRect;
        [UIView transitionWithView:commentBar
                          duration:0.5
                           options:UIViewAnimationTransitionNone
                        animations:^{
                            
                            commentBar.hidden = YES;
                            commentBar.frame = oriRect;
                            
                        } completion:^(BOOL finished){
                            
                        }];
        commentBar.hidden = YES;
        [commentText resignFirstResponder];
        [self becomeFirstResponder];
    }
}




- (IBAction)commentAction:(id)sender {
    commentMode = !commentMode;
    [self showComment:commentMode];
}

- (IBAction)handleTap:(id)sender {
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
//    [self.navigationController popViewControllerAnimated:YES];
    
    if(commentMode){
        commentMode = !commentMode;
        [self showComment:NO];
    }else{
    
        barHidden = !barHidden;
     
        [self.navBar show:!barHidden];
        [self.toolBar show:!barHidden];
    }
}


@end
