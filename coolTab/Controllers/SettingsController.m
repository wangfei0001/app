//
//  SettingsViewController.m
//  coolTab
//
//  Created by fei wang on 28/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "SettingsController.h"


#import "SettingImageCell.h"

#import "SettingNormalCell.h"

#import "SettingSwitchCell.h"

@interface SettingsController ()

@end

@implementation SettingsController{
    NSDictionary *settingsData;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        //settingsData = [[NSMutableArray alloc]initWithCapacity:0];
        
        //group one
        NSDictionary *item0 = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"general", @"id",
                               @"通用设置",@"text",
                               @"normal",@"type",
                               nil];
        
        NSDictionary *item1 = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"privacy", @"id",
                               @"隐私",@"text",
                               @"normal",@"type",
                               nil];
        
        NSDictionary *item2 = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"personalinfo", @"id",
                               @"个人信息",@"text",
                               @"image",@"type",
                               [UIImage imageNamed:@"head.jpeg"], @"image",
                               nil];
        
        NSDictionary *item3 = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"systemmsg", @"id",
                               @"消息通知",@"text",
                               @"switch",@"type",
                               nil];
        
        NSDictionary *group0 = [NSDictionary dictionaryWithObjectsAndKeys:
                                item0, @"item0",
                                item1, @"item1",
                                item2, @"item2",
                                item3, @"item3",
                                nil];
        //group two
        NSDictionary *item4 = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"aboutus", @"id",
                               @"关于我们",@"text",
                               @"normal",@"type",
                               nil];
        NSDictionary *item5 = [NSDictionary dictionaryWithObjectsAndKeys:
                               @"clearcache", @"id",
                               @"清除缓存",@"text",
                               @"normal",@"type",
                               nil];
        NSDictionary *group1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                item4, @"item0",
                                item5, @"item1",
                                nil];
        settingsData = [NSDictionary dictionaryWithObjectsAndKeys:
                        group0, @"group0",
                        group1, @"group1",
                        nil];
//        NSLog(@"%@",settingsData);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    CGRect navFrame = self.navigationController.navigationBar.frame;
    navFrame.origin.x = 0;
    self.navigationController.navigationBar.frame = navFrame;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return settingsData.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *sectionName = [NSString stringWithFormat:@"group%d", section];
    NSDictionary *group =  (NSDictionary *)[settingsData objectForKey:sectionName];
    
    return group.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;

    NSString *sectionName = [NSString stringWithFormat:@"group%d", indexPath.section];
    NSDictionary *group =  (NSDictionary *)[settingsData objectForKey:sectionName];
    
    NSString *itemName = [NSString stringWithFormat:@"item%d", indexPath.row];
    NSDictionary *item = (NSDictionary *)[group objectForKey:itemName];
    
    NSLog(@"%@", item);
    NSString *type = (NSString*)[item objectForKey:@"type"];

    if([type isEqualToString:@"switch"]){
        cell = (SettingSwitchViewCell *)[tableView dequeueReusableCellWithIdentifier:@"settingSwitchCell"];
    }else if([type isEqualToString:@"normal"]){
        cell = (SettingNormalCell *)[tableView dequeueReusableCellWithIdentifier:@"settingNormalCell"];
    }else if([type isEqualToString:@"image"]){
        cell = (SettingImageCell *)[tableView dequeueReusableCellWithIdentifier:@"settingImageCell"];

        UIImage *image = [item objectForKey:@"image"];
        ((SettingImageCell *)cell).rightImage.image =image;
    }
    
    cell.textLabel.text = [item objectForKey:@"text"];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//    
//}





- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
