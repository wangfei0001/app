//
//  PostControllerViewController.m
//  Olutu
//
//  Created by fei wang on 9/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "PostControllerViewController.h"

@interface PostControllerViewController ()

@end

@implementation PostControllerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
