//
//  ChooseImageController.m
//  Olutu
//
//  Created by fei wang on 6/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ChooseImageController.h"

#import <QuartzCore/QuartzCore.h>

#import "PostController.h"

#import "MBProgressHUD.h"

#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

#define DEGREES_RADIANS(angle) ((angle) / 180.0 * M_PI)

@interface ChooseImageController ()

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation ChooseImageController{
    UIActivityIndicatorView *activityIndicator;
}

@synthesize image;

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.imageView.frame =
        CGRectMake(0,0,self.view.frame.size.width, [[UIScreen mainScreen] bounds].size.height - 88);
    self.imageView.image = image;
    NSLog(@"L:%f T:%f", self.imageView.frame.origin.x, self.imageView.frame.origin.y);
    NSLog(@"W:%f H:%f", self.imageView.frame.size.width, self.imageView.frame.size.height);
    
    activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    activityIndicator.hidden = YES;
    activityIndicator.center = self.imageView.center;
    [self.view addSubview: activityIndicator];
    
    [self.navigationController setNavigationBarHidden:NO];
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/***
 * 向左旋转图片
 */
- (IBAction)leftRotate:(id)sender {

    [activityIndicator startAnimating];
    activityIndicator.hidden = NO;
    
    self.image = [self imageRotatedByDegrees:-90.0];
    self.imageView.image = self.image;
    
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:0.5];
    //activityIndicator.hidden = YES;
}



/***
 * 向右旋转图片
 */
- (IBAction)rightRotate:(id)sender {
    [activityIndicator startAnimating];
    activityIndicator.hidden = NO;
    
    self.image = [self imageRotatedByDegrees:90.0];
    self.imageView.image = self.image;
    
    [self performSelector:@selector(hideLoading) withObject:nil afterDelay:0.5];
    //activityIndicator.hidden = YES;

}


- (void)hideLoading{
    activityIndicator.hidden = YES;
}

- (IBAction)submitAction:(id)sender {
}

- (UIImage *)imageRotatedByDegrees:(CGFloat)degrees
{

    
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.image.size.width, self.image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(DEGREES_RADIANS(degrees));
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, DEGREES_RADIANS(degrees));
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-self.image.size.width / 2, -self.image.size.height / 2, self.image.size.width, self.image.size.height), [self.image CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"postimage"]){

        
//        if([self.delegate isKindOfClass:[ViewController class]]){
//            [activityIndicator startAnimating];
//            PostController *pc = [segue destinationViewController];
//
//            pc.image = image;
//        }
    }
}

@end
