//
//  CreateTripController.h
//  coolTab
//
//  Created by fei wang on 9/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBProgressHUD.h"

#import "SNSBar.h"

@interface CreateTripController : UIViewController<UITextFieldDelegate, MBProgressHUDDelegate, snsBarDelegate>

@property (retain, nonatomic) IBOutlet UITextField *tripNameText;

@end
