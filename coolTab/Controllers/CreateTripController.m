//
//  CreateTripController.m
//  coolTab
//
//  Created by fei wang on 9/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "AppDelegate.h"

#import "CreateTripController.h"

#import "TripDetailController.h"

#import "Data.h"

#import "Api.h"

#import "Trip.h"

#import <QuartzCore/QuartzCore.h>

@interface CreateTripController ()

@end

@implementation CreateTripController{
    MBProgressHUD *HUD;
    
    DataShareSetting *shareSetting;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
        shareSetting = [[DataShareSetting alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[self.view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.tripNameText becomeFirstResponder];
    
    self.title = @"创建行程";
    
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithTitle:@"创建" style:UIBarButtonItemStylePlain target:self action:@selector(create:)];
    self.navigationItem.rightBarButtonItem = anotherButton;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)iconClick:(id)sender indentify:(NSString *)indentify index:(int)index selected:(BOOL)selected{
    NSLog(@"click %@ index:%d selected:%d", indentify, index, selected);
    switch(index){
        case 0:
            shareSetting.weibo = [NSNumber numberWithBool:selected];
            break;
        case 1:
            shareSetting.qq = [NSNumber numberWithBool:selected];
            break;
        case 2:
            shareSetting.facebook = [NSNumber numberWithBool:selected];
            break;
        case 3:
            shareSetting.twitter = [NSNumber numberWithBool:selected];
            break;
    }
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    //隐藏键盘，将self变为responder
    [self.tripNameText resignFirstResponder];
    [self becomeFirstResponder];
    
    return YES;
}


- (void)myTask {
    // Do something usefull in here instead of sleeping ...
    sleep(3);
    NSLog(@"done");
}

- (IBAction)create:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    return;
    // The hud will dispable all input on the view (use the higest view possible in the view hierarchy)
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
	
    // Regiser for HUD callbacks so we can remove it from the window at the right time
    HUD.delegate = self;
	[HUD show:YES];
    // Show the HUD while the provided method executes in a new thread
    //[HUD showWhileExecuting:@selector(myTask) onTarget:self withObject:nil animated:YES];
    
    DataTrip *tripData = [[DataTrip alloc]init];
    
    tripData.Name = self.tripNameText.text;
    
    tripData.Share = shareSetting;
    
    tripData.isLocked = [NSNumber numberWithBool:YES];
    
    tripData.isGroup = [NSNumber numberWithBool:NO];
    
    //tripData.groupMembers = nil;
    
    Api *api = [[Api alloc] init];
    
    NSDictionary *result = [api saveTrip:tripData];
    
    if(result.count == 1){
        //for(NSDictionary *resultData in result){
            id value = [result objectForKey:@"result"];
            if([value intValue] > 0){     //success
                NSLog(@"Success");
                [self gotoDetail:[value intValue]];
                return;
            }
        //}
    }
    
    NSLog(@"Failed");
}


- (void)gotoDetail:(int)tripId{
    TripDetailController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tripDetailController"];
    vc.tripId = tripId;
    
    //load trip details
    Trip *trip = [[Trip new]initWithTripId:vc.tripId];
    
//    vc.data = [trip loadPins:1];
//    
//    NSLog(@"Load trip pins: %@ %d", vc.data, vc.data.count);
//    
//    User *_user = myself;
//    
//    [vc.user addObject:_user];
//    
//    [HUD removeFromSuperview];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
}

//- (void)viewWillDisappear:(BOOL)animated
//{
//    [self.navigationController setNavigationBarHidden:YES];
//}

@end
