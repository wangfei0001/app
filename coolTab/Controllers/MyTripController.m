//
//  MyTripController.m
//  Olutu
//
//  Created by fei wang on 15/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "MyTripController.h"

@interface MyTripController ()

@end

@implementation MyTripController{
    NSArray *viewControllers;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    UIViewController *vc1 = [[UIViewController alloc] init];
    UIViewController *vc2 = [[UIViewController alloc] init];
    viewControllers = [NSArray arrayWithObjects:vc1, vc2, nil];
    
    [self.tabBarController setViewControllers:viewControllers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    NSLog(@"hello");
    
    
}

//- (void)tabBarController:(UITabBarController *)ptabBarController didSelectViewController:(UIViewController *)pviewController
//{
//     if (pviewController == [viewControllers objectAtIndex:1]){
//         
//         //	UITabBar *tabBar = ptabBarController.tabBar;
//         //	tabBar.hidden = YES;
//
//         ptabBarController.tabBar.hidden = YES;
//         UIView *mainView = [[viewControllers objectAtIndex:1] view];
//
//         CGRect mainViewFrame = mainView.frame;
//         mainViewFrame.size.height += ptabBarController.tabBar.frame.size.height;
//         mainViewFrame.origin.y = 0;
//         mainView.frame = mainViewFrame;
//     }
//}

@end
