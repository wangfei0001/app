//
//  ModalSegue.m
//  Olutu
//
//  Created by fei wang on 7/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ModalSegue.h"

#import <QuartzCore/QuartzCore.h>

@implementation ModalSegue

- (void) perform {
    UIViewController *dst = (UIViewController *) self.destinationViewController;
    UIViewController *src = (UIViewController *) self.sourceViewController;

 //   dst.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
  
//    CATransition* transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.type = kCATransitionFade;
//    //transition.subtype = kCATransitionFromTop;
//    transition.delegate = src;

    [src presentViewController:dst animated:NO completion:nil];
//    [src.navigationController.view.layer addAnimation:transition forKey:nil];



}

@end
