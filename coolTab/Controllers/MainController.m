//
//  MainController.m
//  Olutu
//
//  Created by fei wang on 10/04/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "MainController.h"

#import "AppDelegate.h"

@interface MainController ()

@end

@implementation MainController{
    BOOL bSlide;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization


    }
    return self;
}


- (IBAction)moveLeftMenu:(id)sender {
    [self moveFloatView:YES];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    bSlide = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    //tabbar
    //UITabBar *tabBar =
    
    [AppDelegate hideTabBar:self.tabBarController];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)getSlideStatus
{
    return bSlide;
}

/***
 * 移动Main Float View to left/right
 * fold: false-向右展开, true-向左收缩
 */
- (void)moveFloatView:(BOOL)animate
{
    float offset;
    CGSize frameSize = [[UIScreen mainScreen] applicationFrame].size;
    
    offset = bSlide ? 0 : frameSize.width - 54.0f;
    
    if(animate){
        [UIView transitionWithView:self.mainView duration:0.2 options:UIViewAnimationOptionTransitionNone animations:^{
            CGRect rect = self.mainView.frame;
            rect.origin.x = offset;
            [self.mainView setFrame:rect];

            CGRect navFrame = self.navigationController.navigationBar.frame;
            navFrame.origin.x = offset;
            self.navigationController.navigationBar.frame = navFrame;
            
        } completion:^(BOOL finished) {
            bSlide = !bSlide;
            
            //self.tripsView.scrollEnabled = !bSlide;
        }];
    }else{
        CGRect rect = self.mainView.frame;
        rect.origin.x = offset;
        [self.mainView setFrame:rect];

        CGRect navFrame = self.navigationController.navigationBar.frame;
        navFrame.origin.x = offset;
        self.navigationController.navigationBar.frame = navFrame;
        
        bSlide = !bSlide;
        
        //self.tripsView.scrollEnabled = !bSlide;
    }

}

@end
