//
//  ProfileController.h
//  coolTab
//
//  Created by fei wang on 8/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "User.h"

#import "ProfileViewCell.h"

#import "BaseController.h"

#import "NavigationBar.h"

@interface ProfileController : BaseController<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic) User *user;

//@property (weak, nonatomic) IBOutlet ProfileViewCell *userProfileCell;
@property (weak, nonatomic) IBOutlet UITableView *profileView;

@property (weak, nonatomic) IBOutlet NavigationBar *navBar;

@property (weak, nonatomic) IBOutlet UIImageView *profileBackgroundImage;

@end
