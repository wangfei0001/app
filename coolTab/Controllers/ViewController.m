//
//  ViewController.m
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//


#import "ViewController.h"

#import "UsersController.h"

#import "CreateTripController.h"

#import "TripDetailController.h"

#import "TripsView.h"

#import "Global.h"

#import "SplashView.h"

#import "Trip.h"

#import "XMLReader.h"

#import "ImagePickerController.h"

#import "ChooseImageController.h"

#import "LeftPanelCell.h"

#import "Menus.h"

#import "AppDelegate.h"

#import "MainController.h"


@interface ViewController ()

@end

@implementation ViewController
{
    AppDelegate *appDelegate;
    
}

@synthesize tripData = _tripData;

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = APP_DELEGATE;
    
    //[self initMenu];
    
    //Global *global = [Global getInstance];
    
    
    //UIWindow *window = [[UIApplication sharedApplication] keyWindow];


    
    
//    UIBarButtonItem *homeBut=[[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"Airplane.png"] style:UIBarButtonItemStylePlain target:self action:@selector(showLeftPanelClicked)];

    
    //NSArray *tabButtons = [[NSArray alloc]initWithObjects:homeBut, nil];
    
    
    //CGRect tripsViewRect = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height - 90);
    id tripsView = [self.tripsView init];
    if(tripsView){
        
    }
    
    //[[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg_tab.png"]];


    //[self.leftPanelView performSelectorOnMainThread:@selector(reload:)
//                                         withObject:nil
//                                      waitUntilDone:NO];
    
    
    //created thread
    //[NSThread detachNewThreadSelector:@selector(initThread) toTarget:self withObject:nil];
    [self initThread];
    
    global.initialize = true;
}




- (void)initThread{
    //download splash images
    
    NSDate *dat = [NSDate date];
    
    NSTimeInterval preTime=[dat timeIntervalSinceNow];
    
    //load trips
    self.tripsView.data = appDelegate.session.tripsData;
    
    [self.tripsView performSelectorOnMainThread:@selector(reloadData)
                                     withObject:nil
                                  waitUntilDone:NO];
    
    NSTimeInterval curTime = [dat timeIntervalSinceNow];
    
    NSTimeInterval timeSpend = curTime - preTime;
    
    
    NSLog(@"Loading time:%f", timeSpend);
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}






/***
 * 准备调用Segue
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([[segue identifier] isEqualToString:@"showtripdetail"]){
        TripDetailController *vc = [segue destinationViewController];
        
        int idx = [self.tripsView indexPathForSelectedRow].row;
        
        id data = [self.tripsView.data objectAtIndex:idx];
        
        vc.tripId = [[data objectForKey:@"id"] intValue];
        
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [self.view addSubview:HUD];
        [HUD show:YES];
        //load trip details
        Trip *trip = [[Trip new]initWithTripId:vc.tripId];
        
        vc.data = [trip loadPins:1];
        
        NSLog(@"Load trip pins: %@ %d", vc.data, vc.data.count);
        
        User *_user = [trip getUser];
        
        [vc.user addObject:_user];
    
        [HUD removeFromSuperview];
        
        NSLog(@"prepareForSegue is called %d Image total:%d", vc.tripId, vc.data.count);
        return;
    }
}






//
//
///***
// * 判断是否应该调用Segue
// */
//-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
//    if([identifier isEqualToString:@"showtripdetail"]){
//        MainController *mainController = (MainController *)self.parentViewController;
//        
//        BOOL shouldSlide = [mainController getSlideStatus] || bFlyingExpanded ? NO : YES;
//        
//        //收缩
//        if(bFlyingExpanded){
//            //[self.tabView flyingButExpand];
//        }
//        
//        return shouldSlide;
//    }
//    return YES;
//}
//






/*
 * 滑动手势
 */
- (IBAction)handleLeftAction:(id)sender {
    MainController *mainController = (MainController *)self.parentViewController;
    
    if([mainController getSlideStatus]){
        [mainController moveFloatView:YES];
        NSLog(@"Left Current");
    }
}
- (IBAction)handleRightAction:(id)sender {
    MainController *mainController = (MainController *)self.parentViewController;
    
    if(![mainController getSlideStatus]){
        [mainController moveFloatView:YES];
        NSLog(@"Right Current");
    }
}

@end
