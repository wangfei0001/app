//
//  MarketController.m
//  Olutu
//
//  Created by fei wang on 13-7-26.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "MarketController.h"

#import "Api.h"

#import "FlightCellView.h"

#import "UIImageView+WebCache.h"

#import "AppDelegate.h"

@interface MarketController (){
    id data;
    
    AppDelegate *appDelegate;
}

@end

@implementation MarketController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = APP_DELEGATE;
    
	// Do any additional setup after loading the view.
    
    data = [[NSMutableArray alloc] initWithCapacity:0];
    
//    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
//                                @"1", @"is_plan",
//                                nil];
    
//    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
//    UIView *topView = window.rootViewController.view;
//    
//    [appDelegate ShowLoading:topView];
    
    [Api call: @"flights"
       method: @"GET"
        param: nil
      success:^(NSURLRequest *request , NSURLResponse *response , id json) {
          
          NSLog(@"%@", json);
          
          BOOL status = [[json valueForKey:@"status"] boolValue];
          if (status) {
              data = [json valueForKey:@"data"];

              [self.mainTableView reloadData];
          }
          else {
              //            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"Login Unsuccessful"
              //                                                           message:@"Please try again"
              //                                                          delegate:NULL
              //                                                 cancelButtonTitle:@"OK"
              //                                                 otherButtonTitles:NULL];
              //
              //            [alert show];
              
          }
          
      }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellName = @"FlightCellView";     //  0
	
	FlightCellView *cell = [tableView dequeueReusableCellWithIdentifier:cellName];      //   1
	if (cell == nil) {
		cell = [[FlightCellView alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier: cellName];    //  2
	}
    //cell.delegate = self;
    
    //cell.imageView
    //    id plan = [planData objectAtIndex:indexPath.row];
    //    cell.imageView.image = [UIImage imageNamed:@"head.jpeg"];
    //    cell.titleLabel.text = [plan objectForKey:@"name"];
    //    cell.descLabel.text = [plan objectForKey:@"desc"];
    //int count = [plan objectForKey:@"count"];
    
    id obj = [data objectAtIndex:indexPath.row];
    
    cell.title.text = [obj objectForKey:@"name"];
    
    cell.price.text = [obj objectForKey:@"price"];
    
    [cell.image setImageWithURL:[NSURL URLWithString:[obj objectForKey:@"image"]]
                  placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [data count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
