//
//  PostController.h
//  Olutu
//
//  Created by fei wang on 9/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>

#import <CoreLocation/CoreLocation.h>

#import "SNSBar.h"

@interface PostController : UIViewController<snsBarDelegate,CLLocationManagerDelegate,UITextViewDelegate,
    UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *geoLabel;

@property (retain, nonatomic) UIImage *image;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (retain, nonatomic) CLLocationManager *locationManager;

@property (weak, nonatomic) IBOutlet UITextView *desciptionText;

@property (weak, nonatomic) IBOutlet UIView *tripNameView;

@property (weak, nonatomic) IBOutlet UILabel *shareLabel;

@property (weak, nonatomic) IBOutlet SNSBar *shareView;

@end
