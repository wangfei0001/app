//
//  ImagePickerController.m
//  Olutu
//
//  Created by fei wang on 4/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ImagePickerController.h"

#import "ChooseImageController.h"

#import "DefaultController.h"

@interface ImagePickerController (){
    UIImage *pickedImage;
}

@end

@implementation ImagePickerController


- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.allowsEditing = NO;
        self.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypePhotoLibrary];

        self.delegate = self;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    pickedImage = [info objectForKey:UIImagePickerControllerOriginalImage];

    ChooseImageController * chooseImageController = (ChooseImageController *)[self.storyboard instantiateViewControllerWithIdentifier:@"chooseImageController"];
    
    chooseImageController.delegate = self;
    
    chooseImageController.image = pickedImage;
    
    [self pushViewController:chooseImageController animated:YES];
    
//    [self performSegueWithIdentifier:@"ShowChooseImage" sender:self];   //chose image, show choose image controller
}


//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
////    if([[segue identifier] isEqualToString:@"ShowChooseImage"]){
////        ChooseImageController *vc = [segue destinationViewController];
////        vc.delegate = self;
////        vc.image = pickedImage;
////    }
//}



- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [viewController.navigationItem setTitle:@"我的相册"];
    // add done button to right side of nav bar
//    UINavigationItem *topItem;
//    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"返回"
//                                                                   style:UIBarButtonItemStylePlain
//                                                                  target:self
//                                                                  action:@selector(back:)];
//    UINavigationBar *bar = navigationController.navigationBar;
//    
//
//    for (UIView *subview in bar.subviews) {
//        //NSLog(@"subviews=%@",subview);
//        NSString *className = [NSString stringWithFormat:@"%@", [subview class]];
//        NSLog(@"class Name=%@", className);
//        if([className isEqualToString:@"UINavigationButton"]){
//            UIButton *cancelButton = (UIButton *)subview;
//            [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
//        }
//    }
//    viewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回"
//                                                                        style:UIBarButtonItemStylePlain
//                                                                            target:self
//                                                                        action:@selector(back:)];

}

- (void)back{
    NSLog(@"halleo");
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
