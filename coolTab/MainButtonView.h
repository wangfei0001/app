//
//  MainButtonView.h
//  coolTab
//
//  Created by fei wang on 11/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FlyingButtonView.h"

@interface MainButtonView : UIImageView

- (MainButtonView *)initView:(NSString *)title
                         normalImage:(UIImage *)normalImage
                          startPoint:(CGPoint) startPoint;

@end
