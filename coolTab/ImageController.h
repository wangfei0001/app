//
//  ImageController.h
//  coolTab
//
//  Created by fei wang on 25/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NavigationBar2.h"

#import "ToolBar2.h"

@interface ImageController : UIViewController<UIGestureRecognizerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (nonatomic, retain) NSMutableArray *data;

@property int selectedIndex;

@property (weak, nonatomic) IBOutlet NavigationBar2 *navBar;

@property (weak, nonatomic) IBOutlet ToolBar2 *toolBar;

@end
