//
//  MainButtonView.m
//  coolTab
//
//  Created by fei wang on 11/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import "MainButtonView.h"



@interface MainButtonView()

//@property(nonatomic, strong) UIImageView *imageView;
@property(nonatomic, strong) UIImage *normalImage;
@property(nonatomic) CGPoint startPoint;

@end




@implementation MainButtonView


- (MainButtonView *)initView:(NSString *)title
                 normalImage:(UIImage *)normalImage
                startPoint:(CGPoint) startPoint{
    //显示图像
    self = [self initWithImage:normalImage
                             highlightedImage:normalImage];
    if(self){
        self.normalImage = normalImage;
        self.startPoint = startPoint;
        

        self.frame = CGRectMake(startPoint.x,
                                          startPoint.y,
                                          self.normalImage.size.width,
                                          self.normalImage.size.height);
        self.userInteractionEnabled = YES;
    }
    return self;    
}



@end
