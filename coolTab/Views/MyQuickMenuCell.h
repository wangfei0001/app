//
//  MyQuickMenuCell.h
//  Olutu
//
//  Created by fei wang on 13-7-23.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyQuickMenuCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuTitle;

@property (weak, nonatomic) IBOutlet UIImageView *menuImage;

@end
