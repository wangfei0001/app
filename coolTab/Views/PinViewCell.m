//
//  PinViewCell.m
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "PinViewCell.h"

#import <QuartzCore/QuartzCore.h>

@implementation PinViewCell{
    CALayer *layer;
    
    UIWebView *loadingView;
    
    BOOL movedLeft;
}


@synthesize delegate;

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        // Initialization code
//        self.contentView.backgroundColor = [UIColor whiteColor];
//        
//        UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchesPoint:)];
//        [self addGestureRecognizer:tapGesture];
//        
////        NSString *filePath=[[NSBundle mainBundle] pathForResource:@"loading" ofType:@"gif"];
////        //==获取gif数据
////        NSData *gifData=[NSData dataWithContentsOfFile:filePath];
////        //==加载gif数据
////        loadingView = [[UIWebView alloc]init];
////        UIImage *image=[UIImage imageWithData:gifData];
////        loadingView.frame = CGRectMake((self.frame.size.width - image.size.width)/2,
////                                       (self.frame.size.height - image.size.height)/2,
////                                       image.size.width,
////                                       image.size.height);
////        [loadingView loadData:gifData MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
////        [self addSubview:loadingView];
//        
         [self initSlideRoundButton];
        

        

        movedLeft = NO;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    //draw a line
    CGContextRef context = UIGraphicsGetCurrentContext();
    //CGContextClearRect(context, rect);
    
    CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
    
    // Draw them with a 2.0 stroke width so they are a bit more visible.
    CGContextSetLineWidth(context, 0.5);
    
    CGContextMoveToPoint(context, 50, 0); //start at this point
    
    CGContextAddLineToPoint(context, 50, rect.size.height); //draw to this point
    
    
    CGContextSetLineWidth(context, 0.3);
    CGContextMoveToPoint(context, 0, rect.size.height);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height);
    
    
    // and now draw the Path!
    CGContextStrokePath(context);
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initSlideRoundButton{
    NSLog(@"initSlideRoundButton");
    UIImage *avatar = [UIImage imageNamed:@"head.jpeg"];
    
    layer = [CALayer layer];
    layer.frame = CGRectMake(10, 10, 28, 28);
    layer.contentsGravity = kCAGravityResizeAspect;
    layer.contents = (id)avatar.CGImage;
    layer.masksToBounds = YES;
    layer.cornerRadius = 14;
//    layer.backgroundColor = [[UIColor whiteColor]CGColor];
//    layer.borderWidth = 2;
    
    [[self layer] addSublayer:layer];

}

- (void)initToolbar{
    CALayer *toolbarLayer = [CALayer layer];
    toolbarLayer.frame = CGRectMake(20, 10, 280, 28);

    CGColorRef dotColor =[UIColor colorWithHue:0 saturation:0 brightness:0.07 alpha:0.6].CGColor;
    
    toolbarLayer.backgroundColor = dotColor;
    
    toolbarLayer.masksToBounds = YES;
    toolbarLayer.cornerRadius = 5;
    
    [[self layer] addSublayer:toolbarLayer];
}

- (void)animationDidStart:(CAAnimation *)anim{
    int a = 0;
    a = 5;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    NSString *animationName = [anim valueForKey:@"AnimationName"];
    if([animationName isEqualToString:@"avatarModeRotate"]) {
        NSLog(@"Avatar=>%f %f", layer.frame.origin.x, layer.frame.origin.y);
    }
    if([animationName isEqualToString:@"pinImageMoveX"]) {
        NSLog(@"Pin Image=>%f %f", self.pinImage.frame.origin.x, self.pinImage.frame.origin.y);
    }
    
}

//- (void)animationDidStop:(NSString *)animID finished:(BOOL)didFinish context:(void *)context{
//    
//    
//    
//    
//}

- (void)touchesPoint:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [(UITableView *)self.superview indexPathForCell: self];
   
    CGPoint locationInView = [gestureRecognizer locationInView:self];
    //presentationLayer layer的动画层
    CALayer *layer1=[layer hitTest:locationInView];
    NSLog(@"Touch point index:%d X:%f Y:%f", indexPath.row, locationInView.x, locationInView.y);
    NSLog(@"Layer pos %f %f", layer.frame.origin.x, layer.frame.origin.y);
    if (layer1!=nil) {
        NSLog(@"点击了运动的layer");
        
        float duration = 0.5;
        
        CGPoint point1;
        
        if(!movedLeft){
            point1 = CGPointMake(layer.position.x - 2 * M_PI * 28,
                                     layer.position.y);
        }else{
            point1 = CGPointMake(layer.position.x + 2 * M_PI * 28,
                                 layer.position.y);
        }
        CABasicAnimation *avatarMoveX = [CABasicAnimation animationWithKeyPath:
                                    @"position"];
        avatarMoveX.fromValue = [layer valueForKey:@"position"];
        avatarMoveX.toValue = [NSValue valueWithCGPoint:point1];
        layer.position = point1;
        avatarMoveX.removedOnCompletion = NO;
        
        
        CABasicAnimation* avatarRotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        avatarRotate.duration = 0.5;
        avatarRotate.timingFunction = [CAMediaTimingFunction functionWithName: kCAMediaTimingFunctionLinear];
        avatarRotate.cumulative = YES;
        avatarRotate.toValue = [NSNumber numberWithFloat:2 * M_PI];
        
        NSMutableArray* animationsArray = [NSMutableArray arrayWithObjects:avatarMoveX,
                                           avatarRotate,
                                           nil];
        CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
        animationGroup.duration = duration;
        //animationGroup.timingFunction = [CAMediaTimingFunctionfunctionWithName:kCAMediaTimingFunctionEaseIn];
        animationGroup.animations = animationsArray;
        animationGroup.delegate = self;
        animationGroup.removedOnCompletion = NO;
        animationGroup.fillMode = kCAFillModeForwards ;
        [animationGroup setValue:@"avatarModeRotate" forKey:@"AnimationName"];
        [layer addAnimation:animationGroup forKey:@"avatarModeRotate"];
 
        CGPoint point2;
        
        if(!movedLeft){
            point2 = CGPointMake(self.pinImage.layer.position.x - 2 * M_PI * 28,
                                        self.pinImage.layer.position.y);
        }else{
            point2 = CGPointMake(self.pinImage.layer.position.x + 2 * M_PI * 28,
                                 self.pinImage.layer.position.y);
        }
        CABasicAnimation *pinImageMoveX = [CABasicAnimation animationWithKeyPath:@"position"];
        //this is not used, as the group provides the duration
        pinImageMoveX.duration = duration;
        pinImageMoveX.delegate = self;
        pinImageMoveX.fromValue = [self.pinImage.layer valueForKey:@"position"];
        pinImageMoveX.toValue = [NSValue valueWithCGPoint:point2];
        self.pinImage.layer.position = point2;
        pinImageMoveX.fillMode = kCAFillModeForwards ;
        pinImageMoveX.removedOnCompletion = NO;
        [pinImageMoveX setValue:@"pinImageMoveX" forKey:@"AnimationName"];
        [self.pinImage.layer addAnimation:pinImageMoveX forKey:@"pinImageMoveX"];
        
        movedLeft = !movedLeft;
        
        return;
    }
    
    CALayer *layer2 = [self.pinImage.layer hitTest:locationInView];
    if(layer2 && !layer1){
        NSLog(@"click image");
        [self.delegate imageClick:self indexPath:indexPath];
    }
    
}


@end
