//
//  NavigationBar.m
//  coolTab
//
//  Created by fei wang on 22/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>


#import "NavigationBar.h"



@implementation NavigationBar

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self) {
    }
    return self;
}

//- (void)layoutSubviews{
//    [super layoutSubviews];
//    
//    CGRect frame = self.frame;
//    frame.size.height = 36;
//    [self setFrame:frame];
//    
//    int i = 0;
//    for (UIView *view in self.subviews) {
//        NSLog(@"%i. %@", i++, [view description]);
//        //if ([view isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
//        //    view.frame = CGRectMake(view.frame.origin.x, 5, view.frame.size.width, 24);
//            
//        //}
//    }
//
//}

//
//- (CGSize)sizeThatFits:(CGSize)size{
//    return CGSizeMake(320, 20);
//}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    UIColor *color = [UIColor colorWithRed:139.0/255 green:178.0/255 blue:38.0/255 alpha:1];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColor(context, CGColorGetComponents( [color CGColor]));
    CGContextFillRect(context, rect);
    [self setBarStyle:UIBarStyleBlack];
    [self setTintColor:color];
    
    //遮盖白边
//    UIView *navBorder = [[UIView alloc] initWithFrame:CGRectMake(0,self.frame.size.height,self.frame.size.width, 1)];
//    [navBorder setBackgroundColor:color];
//    [self addSubview:navBorder];
    
}


-(void)willMoveToWindow:(UIWindow *)newWindow {
    [super willMoveToWindow:newWindow];
    [self applyShadowStyle];
}



-(void)applyShadowStyle{
    // add the drop shadow
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOffset = CGSizeMake(0.0, 3);
    self.layer.shadowOpacity = 0.25;
    self.layer.masksToBounds = NO;
    self.layer.shouldRasterize = YES;
}

@end
