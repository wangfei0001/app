//
//  TripsView.h
//  coolTab
//
//  Created by fei wang on 9/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol tripsViewDelegate
//-(void)singleTripClick:(id)sender
//              TripId:(int)tripId;
//@end

@interface TripsView : UITableView<UITableViewDelegate,UITableViewDataSource>

- (void)ShowOverlay;

- (void)HideOverlay;

@property (nonatomic, retain) NSMutableArray* data;

//@property (nonatomic, assign) id<tripsViewDelegate> controllerDelegate;


@end
