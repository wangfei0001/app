//
//  MyTripCell.m
//  Olutu
//
//  Created by fei wang on 5/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "MyTripCell.h"

@implementation MyTripCell

-(id)init{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"tripcell" owner:nil options:nil];
    
    self = [subviewArray objectAtIndex:0];
    
    if (self) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        self.userInteractionEnabled = YES;
        
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


//- (void)drawRect:(CGRect)rect{
//    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    self.layer.borderWidth = 1.0f;
//}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
