//
//  PullTableFooter.h
//  coolTab
//
//  Created by fei wang on 21/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PullTableFooter : UITableViewCell

- (void)startLoading:(BOOL)loading;

@property (retain, nonatomic) IBOutlet UILabel *moreLabel;

@property (retain, nonatomic) IBOutlet UIWebView *loadingImage;

@end
