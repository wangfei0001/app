//
//  ProfileViewCell.h
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol userProfileViewDelegate<NSObject>

-(void)avatarClick:(id)sender;

@end

@interface ProfileViewCell : UITableViewCell{
    
}

- (void)initAvatarImage;

-(void)setBackground:(UIImage *)image;

@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (nonatomic, assign) id<userProfileViewDelegate> delegate;

@end
