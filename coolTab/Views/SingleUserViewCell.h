//
//  SimpleTableCell.h
//  TableView2
//
//  Created by fei wang on 4/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleUserViewCell : UITableViewCell

- (void)toggleEditMode:(Boolean)editMode;

@property (nonatomic,retain) IBOutlet UILabel *nameLabel;

@property (nonatomic,retain) IBOutlet UILabel *prepTimeLabel;

@property (nonatomic,retain) IBOutlet UIImageView *thumbnailImageView;

@end
