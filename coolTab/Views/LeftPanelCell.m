//
//  LeftPanelCell.m
//  Olutu
//
//  Created by fei wang on 7/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "LeftPanelCell.h"

@implementation LeftPanelCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"leftpanelcell" owner:self options:nil];
        
        self.mainView = (LeftPanelCellView *)[subviewArray objectAtIndex:0];
        
        [self addSubview:self.mainView];

        
        CGFloat nRed=56.0/255.0;
        CGFloat nBlue=56.0/255.0;
        CGFloat nGreen=56.0/255.0;
        UIColor *myColor=[[UIColor alloc]initWithRed:nRed green:nBlue blue:nGreen alpha:1];
        
        UIView *bgColorView = [[UIView alloc] init];
        [bgColorView setBackgroundColor:myColor];
        [self setSelectedBackgroundView:bgColorView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
