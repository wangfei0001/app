//
//  SplashView.h
//  coolTab
//
//  Created by fei wang on 2/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashView : UIView{
    IBOutlet UIImageView *contentImage;
    
}

- (void)sendContent:(NSArray *)params;

@property (nonatomic, retain) IBOutlet UIImageView *contentImage;

@property BOOL animationDone;

@end
