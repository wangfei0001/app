//
//  FlightCellView.h
//  Olutu
//
//  Created by fei wang on 13-7-27.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightCellView : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *expired;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@end
