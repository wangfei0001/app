//
//  PlanCell.m
//  Olutu
//
//  Created by fei wang on 13-7-23.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "PlanCell.h"

@implementation PlanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSIndexPath *indexPath = [(UITableView *)self.superview indexPathForCell: self];
    
    UITouch *touch = touches.anyObject;
    
    if(touch.view == self.planTypeImage){
        [self.delegate imageClick:self indexPath:indexPath];
//        if([self.delegate respondsToSelector:@selector(imageClick:indexPath:)]){
//            NSLog(@"");
//        }else{
//            NSLog(@"Delegate class didn't implement this method!");
//        }
    }
}

@end
