//
//  ProfileViewCell.m
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ProfileViewCell.h"

@implementation ProfileViewCell

-(id)init{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"profilecell" owner:self options:nil];
    self = [subviewArray objectAtIndex:0];
    if (self) {
        self.userInteractionEnabled = YES;
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        

    }
    return self;
}

//
//- (void)drawRect:(CGRect)rect {
//
//    self.backgroundColor = [UIColor blackColor];
//}

- (void)initAvatarImage{
    UIImage *avatar = [UIImage imageNamed:@"head.jpeg"];
    
    CALayer *layer = [CALayer layer];
    
    //[layer setBackgroundColor:[[UIColor whiteColor] CGColor]];
    
    layer.frame = CGRectMake(0, 0, avatar.size.width, avatar.size.height);
    layer.contentsGravity = kCAGravityResizeAspect;
    layer.contents = (id)avatar.CGImage;
    layer.borderColor = [[UIColor whiteColor] CGColor];
    layer.borderWidth = 3;
    layer.masksToBounds = YES;
    layer.cornerRadius = avatar.size.width / 2;
    
    //
    
    //    [layer setCornerRadius:25.0f];
    //    [layer setBounds:CGRectMake(0.0f, 0.0f, 50.0f, 50.0f)];
    
    [[self.avatarImage layer] addSublayer:layer];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setBackground:(UIImage *)image{
    self.backgroundImageView = [[UIImageView alloc]initWithImage:image];
    self.backgroundImageView.frame = CGRectMake(0, -60, 320, 240);
    self.backgroundImageView.contentMode = UIViewContentModeTop;
    //self.backgroundImageView.backgroundColor = [UIColor clearColor];
    self.backgroundImageView.layer.zPosition = -100;
    
    self.backgroundView = [[UIView alloc]initWithFrame:self.frame];
    
    [self.backgroundView addSubview:self.backgroundImageView];
    self.backgroundView.layer.zPosition = -1;
}


//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
//    CGPoint tappedPt = [[touches anyObject] locationInView: self];
//    int     xPos = tappedPt.x;
//    int     yPos = tappedPt.y;
//    NSLog(@"%d %d", xPos, yPos);
//    NSLog(@"Back %f %f", self.backgroundView.frame.origin.x, self.backgroundView.frame.origin.y);
//}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = touches.anyObject;
    
    if(touch.view == self.avatarImage){
        if([self.delegate respondsToSelector:@selector(avatarClick:)]){
            [self.delegate avatarClick:self];
        }else{
            NSLog(@"Delegate class didn't implement this method!");
        }
        
        //        UIStoryboard *st = [UIStoryboard storyboardWithName:[[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"] bundle:[NSBundle mainBundle]];
        //
        //        UIViewController *myController = [st instantiateViewControllerWithIdentifier:@"ProfileController"];
        //        UINavigationController *navController = [st instantiateViewControllerWithIdentifier:@"NavigationController"];
        //
        //
        //
        //        [navController pushViewController: myController animated:YES];
        
        
        //
        //        CreateTripController * createController = (CreateTripController *)[self.storyboard instantiateViewControllerWithIdentifier:@"createTripController"];
        //
        //        UINavigationController *navigationController = (UINavigationController *)[self.storyboard instantiateViewControllerWithIdentifier:@"NavigationController"];
        //        id nav = [navigationController initWithRootViewController:createController];
        //        if(nav){
        //
        //        }
        //        [self presentViewController:navigationController animated:YES completion: nil];
    }
}


@end
