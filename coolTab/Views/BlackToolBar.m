//
//  BlackToolBar.m
//  Olutu
//
//  Created by fei wang on 9/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "BlackToolBar.h"

#import <QuartzCore/QuartzCore.h>

@implementation BlackToolBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)init{
    self = [super init];
    if(self){
        self.frame = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height -44, 320, 44);
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        //self.frame = CGRectMake(0,[[UIScreen mainScreen] bounds].size.height -44, 320, 44);
        self.barStyle = UIBarStyleBlackTranslucent;
        self.backgroundColor = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.6];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    UIColor *black = [[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.6];
//    
//    [black set];
//    CGContextFillRect(UIGraphicsGetCurrentContext(), rect);
//    NSLog(@"ToolBar=>L:%f T:%f", self.frame.origin.x, self.frame.origin.y);
//    NSLog(@"ToolBar=>W:%f H:%f", self.frame.size.width, self.frame.size.height);
//}


@end
