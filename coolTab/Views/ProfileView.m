//
//  ProfileView.m
//  coolTab
//
//  Created by fei wang on 22/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ProfileView.h"

#import "ProfileViewCell.h"

#import "MyTripCell.h"

@implementation ProfileView

@synthesize user;

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self) {
//        // Initialization code
//    }
//    return self;
//}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.delegate = self;
        self.dataSource = self;
        
        self.separatorColor = [UIColor clearColor];
    }
    return self;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){//profile cell
        static NSString *CellIdentifier = @"profileCell";
        ProfileViewCell *cell = (ProfileViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        [cell.profileView.userNameLabel setText:self.user.screenName];
        [cell.profileView initAvatarImage];
        [cell.profileView initBackground];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }else{
        static NSString *CellIdentifier = @"myTripCell";
        
        MyTripCell *cell = (MyTripCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 125 + 4;
    }else
        return 82  + 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
