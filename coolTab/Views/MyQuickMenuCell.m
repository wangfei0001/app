//
//  MyQuickMenuCell.m
//  Olutu
//
//  Created by fei wang on 13-7-23.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "MyQuickMenuCell.h"

@implementation MyQuickMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
