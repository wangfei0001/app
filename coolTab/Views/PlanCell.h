//
//  PlanCell.h
//  Olutu
//
//  Created by fei wang on 13-7-23.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol planCellViewDelegate//<NSObject>

-(void)imageClick:(id)sender indexPath:(NSIndexPath *)indexPath;

@end

@interface PlanCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *DisImage;

@property (weak, nonatomic) IBOutlet UILabel *destination;

@property (weak, nonatomic) IBOutlet UIImageView *planTypeImage;


@property (nonatomic, assign) id<planCellViewDelegate> delegate;

@end
