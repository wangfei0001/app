//
//  SettingSwitchViewCell.m
//  Olutu
//
//  Created by fei wang on 14/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "SettingSwitchCell.h"

@implementation SettingSwitchViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
