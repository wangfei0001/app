//
//  SNSBar.h
//  Olutu
//
//  Created by fei wang on 11/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>


#define SNS_WEBSITE_WEIBO    @"weibo"

#define SNS_WEBSITE_TENGXUN  @"tengxun"

#define SNS_WEBSITE_FACEBOOK @"facebook"

#define SNS_WEBSITE_TWITTER  @"twitter"


@protocol snsBarDelegate

- (void)iconClick:(id)sender indentify:(NSString *)indentify index:(int)index selected:(BOOL)selected;

@end

@interface SNSBar : UIView{
    NSObject<snsBarDelegate>  *delegate;
}

@property (readwrite, retain) IBOutlet NSObject<snsBarDelegate> *delegate;

@end
