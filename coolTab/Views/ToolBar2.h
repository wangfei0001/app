//
//  ToolBar2.h
//  coolTab
//
//  Created by fei wang on 1/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToolBar2 : UIToolbar

- (void)show:(BOOL)needShow;

@end
