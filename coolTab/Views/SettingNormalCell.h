//
//  SettingNormalCell.h
//  Olutu
//
//  Created by fei wang on 14/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingNormalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightTextLabel;

@end
