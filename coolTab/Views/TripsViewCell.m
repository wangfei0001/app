//
//  TripsViewCell.m
//  coolTab
//
//  Created by fei wang on 9/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "TripsViewCell.h"

@implementation TripsViewCell{
    UIActivityIndicatorView *loadingView;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        // Initialization code
        self.frame = CGRectMake(6, 6, 308, 160);
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self.imageView setClipsToBounds:YES];

        loadingView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        loadingView.center = self.center;
        [loadingView startAnimating];
        [self addSubview:loadingView];
    }
    return self;
}


- (void)loadingDone{
    [loadingView removeFromSuperview];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
