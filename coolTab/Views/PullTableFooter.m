//
//  PullTableFooter.m
//  coolTab
//
//  Created by fei wang on 21/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "PullTableFooter.h"

#define FOOT_LABEL_WIDTH 100

@implementation PullTableFooter

@synthesize moreLabel = _moreLabel;

@synthesize loadingImage = _loadingImage;


- (id)initWithFrame:(CGRect)frame
{
    NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"dropdownfooter" owner:nil options:nil];
    
    self = [subviewArray objectAtIndex:0];
    if(self){
        self.frame = frame;
        [self loadIndicator];
    }
    return self;
}


- (void)loadIndicator
{
//    NSString *filePath=[[NSBundle mainBundle] pathForResource:@"loading" ofType:@"gif"];
//    //==获取gif数据
//    NSData *gifData=[NSData dataWithContentsOfFile:filePath];
//    //==加载gif数据
//    self.loadingImage = [[UIWebView alloc]init];
//    UIImage *image=[UIImage imageWithData:gifData];
//    self.loadingImage.frame = CGRectMake(self.loadingImage.frame.origin.x,
//                                         self.loadingImage.frame.origin.y,
//                                         image.size.width,
//                                         image.size.height);
//    [self.loadingImage loadData:gifData MIMEType:@"image/gif" textEncodingName:nil baseURL:nil];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(
                            self.moreLabel.frame.origin.x + self.moreLabel.frame.size.width + 5,
                            self.moreLabel.frame.origin.y,
                             20,
                             20
                               );
    spinner.hidesWhenStopped = YES;
    [spinner startAnimating];
    [self addSubview:spinner];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)startLoading:(BOOL)loading{
    if(loading){
        [self.moreLabel setText:@"正在加载"];
        self.loadingImage.hidden = NO;
    }else{
        [self.moreLabel setText:@"下拉显示更多"];
        self.loadingImage.hidden = YES;
    }
}

@end
