//
//  ProcessLoading.h
//  coolTab
//
//  Created by fei wang on 10/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProcessLoadingDelegate;

@interface ProcessLoading : UIView{

    id<ProcessLoadingDelegate> delegate;

}


- (id)initWithView: (UIView *)view;

@property (assign) id<ProcessLoadingDelegate> delegate;

@end

@protocol ProcessLoadingDelegate <NSObject>



@end
