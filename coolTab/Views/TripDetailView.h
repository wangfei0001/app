//
//  TripDetailView.h
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripDetailView : UIView<UITableViewDelegate,UITableViewDataSource>{
    //NSMutableArray *data;

}

@property (nonatomic, retain) NSMutableArray* data;

@property (nonatomic, retain) NSMutableArray* user;

@property (weak, nonatomic) IBOutlet UITableView *detailTable;

@property (nonatomic, assign) id delegate;

@end
