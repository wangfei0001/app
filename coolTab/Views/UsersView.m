//
//  UsersView.m
//  coolTab
//
//  Created by fei wang on 7/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "UsersView.h"

#import "SingleUserViewCell.h"

#import "User.h"

@implementation UsersView{
    Boolean editMode;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        // Initialization code
        self.dataSource = self;
        self.delegate = self;
        
        self.allowsSelection = true;
        self.allowsSelectionDuringEditing = true;
        
        
        
        editMode = FALSE;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SingleUserCell";


    SingleUserViewCell *cell = (SingleUserViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

    if (cell == nil) {
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        //NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SingleUserCell" owner:self options:nil];
        //cell = [nib objectAtIndex:0];
    }


    User *thisUser = [self.users objectAtIndex:indexPath.row];

    NSLog(@"Screen = %@", thisUser.screenName);
    NSLog(@"Gender=%d", thisUser.gender?1:0);
    

    cell.nameLabel.text = thisUser.screenName;
    //cell.thumbnailImageView.image = [UIImage imageNamed:@"head.jpeg"];
    
    
    NSURL *imgURL=[[NSURL alloc]initWithString:thisUser.avatar];
    
    NSData *imgdata=[[NSData alloc]initWithContentsOfURL:imgURL];
    
    UIImage *image=[[UIImage alloc]initWithData:imgdata];
    
    cell.thumbnailImageView.image = image;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.users.count;
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}
//
//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.users removeObjectAtIndex:indexPath.row];
    [self reloadData];
}

- (void)toggleEditMode{
    
    editMode = !editMode;

    for(NSInteger i = 0; i < self.numberOfSections; i++){
        NSIndexPath *loopPath = [NSIndexPath indexPathForRow:i inSection:0];
        
        SingleUserViewCell *cell = (SingleUserViewCell *)[self cellForRowAtIndexPath:loopPath];
        [cell toggleEditMode:editMode];
    }

    [self setEditing:editMode?YES:NO animated:YES];

}


@end
