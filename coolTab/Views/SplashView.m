//
//  SplashView.m
//  coolTab
//
//  Created by fei wang on 2/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "SplashView.h"

@implementation SplashView{
    UIImageView *mainImage;
    
    NSMutableArray *images;
    
    NSMutableArray *durations;
    
    int showdIndex;
}

@synthesize contentImage;

@synthesize animationDone;

- (id)init{
    self = [super init];
    if(self){
        mainImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"bg_pattern_wood.png"]];
        [self addSubview:mainImage];
        
        self.contentImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 240)];
        self.contentImage.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.contentImage];
        
        self.animationDone = NO;

    }
    return self;
}



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)showImage{
    UIImage *image = [images objectAtIndex:showdIndex];
    float duration = [[durations objectAtIndex:showdIndex] floatValue];
    
    NSLog(@"show image %d duration:%f", showdIndex, duration);
    self.contentImage.alpha = 0;
    self.contentImage.image = image;
    [UIView animateWithDuration:1.5 delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^void(){
                         self.contentImage.alpha = 1;
                     }
                     completion:^void(BOOL finished){
                        if(showdIndex < images.count){
                            sleep(duration);
                            [UIView transitionWithView:self.contentImage duration:1 options:UIViewAnimationOptionTransitionNone animations:^{
                                self.contentImage.alpha = 0;
                            } completion:^(BOOL finished) {
                                NSLog(@"Done show image %d", showdIndex);
                                [self showImage];
                            }];
                        }else{
                             self.animationDone = YES;
                        }
                     }];
 
    showdIndex++;
}

- (void)sendContent:(NSArray *)params{
    images = [params objectAtIndex:0];
    durations = [params objectAtIndex:1];
    
    [self showImage];

}


@end
