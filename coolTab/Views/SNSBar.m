//
//  SNSBar.m
//  Olutu
//
//  Created by fei wang on 11/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "SNSBar.h"

#define SNSBAR_MARGIN_BETWEEN_ICONS 10

#define SNSBAR_ICONS_WIDTH 24

#define SNSBAR_ICONS_HEIGHT 24

@implementation SNSBar{
    NSArray *enabledImages;
    NSArray *disabledImages;
    NSArray *names;
    
    
    NSMutableArray *status;
    
    
    
    NSMutableArray *enabledImageViews;
    NSMutableArray *disabledImageViews;
}

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code        
        [self initSubIcons];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self initSubIcons];
    }
    return self;
}


- (void)initSubIcons{
    enabledImages = [[NSArray alloc]initWithObjects:@"weibo_24x24.png",
                     @"tengxun_24x24.png",
                     @"facebook_24x24.png",
                     @"twitter_24x24.png",
                     nil];
    
    disabledImages = [[NSArray alloc]initWithObjects:@"weibo_24x24_disabled.png",
                      @"tengxun_24x24_disabled.png",
                      @"facebook_24x24_disabled.png",
                      @"twitter_24x24_disabled.png",
                      nil];
    
    names = [[NSArray alloc]initWithObjects:SNS_WEBSITE_WEIBO, SNS_WEBSITE_TENGXUN, SNS_WEBSITE_FACEBOOK, SNS_WEBSITE_TWITTER, nil];
    
    enabledImageViews = [[NSMutableArray alloc]initWithCapacity:enabledImages.count];
    disabledImageViews = [[NSMutableArray alloc]initWithCapacity:enabledImages.count];
    
    NSNumber *noObj = [NSNumber numberWithBool: NO];
    status = [[NSMutableArray alloc]initWithObjects:noObj, noObj, noObj, noObj, nil];
    
    float x = 0;
    
    for(int i = 0; i < enabledImages.count; i++){
        NSString *imagePath = [NSString stringWithFormat:@"sns/%@", disabledImages[i]];
        UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:imagePath]];
        [imageView setUserInteractionEnabled:YES];
        imageView.frame = CGRectMake(x, 0, SNSBAR_ICONS_WIDTH, SNSBAR_ICONS_HEIGHT);
        
        x += SNSBAR_ICONS_WIDTH + SNSBAR_MARGIN_BETWEEN_ICONS;
        
        [enabledImageViews addObject:imageView];
        
        [self addSubview:imageView];
    }
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = touches.anyObject;
    
    for(int i = 0; i < enabledImageViews.count; i++){
        if(touch.view == [enabledImageViews objectAtIndex:i]){
            UIImageView *imageView = [enabledImageViews objectAtIndex:i];
            
            NSNumber *sts = status[i];
            NSString *imagePath;
            if([sts boolValue] == YES){
                imagePath = [NSString stringWithFormat:@"sns/%@", disabledImages[i]];
                imageView.image = [UIImage imageNamed:imagePath];
                [status replaceObjectAtIndex:i withObject:[NSNumber numberWithBool: NO]];
            }else{
                imagePath = [NSString stringWithFormat:@"sns/%@", enabledImages[i]];
                imageView.image = [UIImage imageNamed:imagePath];
                [status replaceObjectAtIndex:i withObject:[NSNumber numberWithBool: YES]];
            }
            [self.delegate iconClick:self indentify:names[i] index:i selected:![sts boolValue]];
            break;
        }
    }
    
}

//// Only override drawRect: if you perform custom drawing.
//// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    // Drawing code
//}


@end
