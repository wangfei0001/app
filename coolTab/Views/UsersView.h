//
//  UsersView.h
//  coolTab
//
//  Created by fei wang on 7/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsersView : UITableView<UITableViewDelegate, UITableViewDataSource>

- (void)toggleEditMode;

@property (nonatomic, retain) NSMutableArray* users;

@end
