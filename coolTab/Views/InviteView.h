//
//  InviteView.h
//  coolTab
//
//  Created by fei wang on 23/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteView : UITableView<UITableViewDelegate, UITableViewDataSource>

@end
