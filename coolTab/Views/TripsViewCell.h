//
//  TripsViewCell.h
//  coolTab
//
//  Created by fei wang on 9/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TripsViewCell : UITableViewCell

- (void)loadingDone;

@property (retain, nonatomic) IBOutlet UIImageView *previewImage;

@property (retain, nonatomic) IBOutlet UILabel *descriptionLabel;

@property (retain, nonatomic) IBOutlet UILabel *dateLabel;

@end
