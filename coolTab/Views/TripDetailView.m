//
//  TripDetailView.m
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "TripDetailView.h"

#import "ProfileViewCell.h"

#import "PinViewCell.h"

//#import "UIImageView+WebCache.h"

@implementation TripDetailView

@synthesize data;

@synthesize user;

@synthesize delegate = _delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 130;
    }else
        return 180 + 30;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row > 0){
        static NSString *CellIdentifier = @"pinCell";
        
        PinViewCell *cell = (PinViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            //        cell = [[[TripsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
//            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TripsViewCell" owner:self options:nil];
//            cell = [nib objectAtIndex:0];
        }
        
        id trip = [self.data objectAtIndex:indexPath.row];
        
        
        NSURL *imgURL=[[NSURL alloc]initWithString:[trip objectForKey:@"thumb"]];
        NSData *imgdata=[[NSData alloc]initWithContentsOfURL:imgURL];
        UIImage *image=[[UIImage alloc]initWithData:imgdata];
        cell.pinImage.image = image;
        
        CGRect a = cell.pinImage.frame;
        //NSLog(@"%@", cell.pinImage.frame);
        
        // Here we use the new provided setImageWithURL: method to load the web image
        //[cell.pinImage setImageWithURL:[NSURL URLWithString:[trip objectForKey:@"image"]]
        //              placeholderImage:[UIImage imageNamed:@"demo.jpg"]];
        
        //    [cell.previewImage sizeToFit];
        cell.timeLabel.text = [trip objectForKey:@"time"];
        //cell.descriptionLabel.text = [trip objectForKey:@"desc"];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }else{
        static NSString *CellIdentifier = @"profileCell";
        ProfileViewCell *cell = (ProfileViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        NSLog(@"%@",self.user);
        
        cell.profileView.delegate = self.delegate;
        
        //[cell.profileView.userNameLabel setText:[self.user ]];
        [cell.profileView initAvatarImage];
        [cell.profileView initBackground];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
