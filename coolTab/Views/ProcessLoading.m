//
//  ProcessLoading.m
//  coolTab
//
//  Created by fei wang on 10/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ProcessLoading.h"



@implementation ProcessLoading


@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithView: (UIView *)view
{
    id me = [self initWithFrame:view.bounds];
    //B向A发送消息，则由A来注册该消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    return me;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
