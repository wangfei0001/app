//
//  ToolBar2.m
//  coolTab
//
//  Created by fei wang on 1/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "ToolBar2.h"

@implementation ToolBar2

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
//        const float colorMask[6] = {222, 255, 222, 255, 222, 255};
//        UIImage *img = [[UIImage alloc] init];
//        UIImage *maskedImage = [UIImage imageWithCGImage: CGImageCreateWithMaskingColors(img.CGImage, colorMask)];
//        
//        [self setBackgroundImage:maskedImage forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];

    }
    return self;
}

- (void)show:(BOOL)needShow{
    [UIView transitionWithView:self
                      duration:0.6
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        self.hidden = !needShow;
                    } completion:^(BOOL finished){
                        
                    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
