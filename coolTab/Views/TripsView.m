//
//  TripsView.m
//  coolTab
//
//  Created by fei wang on 9/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "TripsView.h"

#import "TripsViewCell.h"

#import "PullTableFooter.h"

#import "UIImageView+WebCache.h"

#import "Trip.h"

#import "MBProgressHUD.h"

@implementation TripsView{
    CALayer *overlayLayer;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self){
        // Initialization code
        self.dataSource = self;
        self.delegate = self;
        
        static NSString *cellIdentifier = @"tripsViewFooter";
        
        PullTableFooter *cell = (PullTableFooter *)[self dequeueReusableCellWithIdentifier:cellIdentifier];
        
        self.tableFooterView = cell;
        
        //clear cache
        //[[SDImageCache sharedImageCache] clearDisk];
        
        self.showsVerticalScrollIndicator = NO;
    }
    return self;
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if(scrollView.contentOffset.y > ((scrollView.contentSize.height - scrollView.frame.size.height))){
        [NSThread detachNewThreadSelector:@selector(loadMoreData) toTarget:self withObject:nil];
    }
}

- (void)loadMoreData{
//    MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self];
//    //HUD.labelText = @"加载数据";
//    [self addSubview:HUD];
//    [HUD show:YES];
    
    PullTableFooter *footer = (PullTableFooter *)self.tableFooterView;
    [footer startLoading:true];
    //load data from server;
    
    NSMutableArray *newData = [Trip loadTrips];
    
    if(newData){
        for(int i = 0; i < newData.count; i++){
            
            [self.data addObject:[newData objectAtIndex:i]];
        }
        [self performSelectorOnMainThread:@selector(reloadData)
                                         withObject:nil
                                      waitUntilDone:NO];
    }
//    [HUD removeFromSuperview];
}


- (BOOL)allowsFooterViewsToFloat{
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TripsViewCell";
    
    TripsViewCell *cell = (TripsViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:CellIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }else{
        //cell = [[TripsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    }
    
    id trip = [self.data objectAtIndex:indexPath.row];
    
    
//    NSURL *imgURL=[[NSURL alloc]initWithString:[trip objectForKey:@"image"]];
//    NSData *imgdata=[[NSData alloc]initWithContentsOfURL:imgURL];
//    UIImage *image=[[UIImage alloc]initWithData:imgdata];
//    cell.previewImage.image = image;
    
    NSURL *imgURL=[[NSURL alloc]initWithString:[trip objectForKey:@"image"]];
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadWithURL:imgURL options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
        
        [UIView transitionWithView:cell.previewImage
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [cell loadingDone];
                            cell.previewImage.image = image;
                        } completion:^(BOOL finished){
                            
                        }];
    }];
//    [cell.previewImage setImageWithURL:imgURL
//                   placeholderImage:nil];
//    [cell loadingDone];
    
    cell.dateLabel.text = [trip objectForKey:@"date"];
    cell.descriptionLabel.text = [trip objectForKey:@"name"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int defaultHeight = 160;
    
    id trip = [self.data objectAtIndex:indexPath.row];
    
    defaultHeight = [[trip objectForKey:@"height"] intValue] + 30;
    
    return defaultHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data.count;
}

- (void)ShowOverlay{
    [self setContentOffset:self.contentOffset animated:NO];
    self.scrollEnabled = NO;
    
    overlayLayer = [CALayer layer];
    overlayLayer.frame = self.bounds;
    overlayLayer.contentsGravity = kCAGravityResizeAspect;
    overlayLayer.backgroundColor = [UIColor blackColor].CGColor;
    overlayLayer.opacity = 0.6;
    
    [self.layer addSublayer:overlayLayer];
}

- (void)HideOverlay{
    [overlayLayer removeFromSuperlayer];
    self.scrollEnabled = YES;
}

@end
