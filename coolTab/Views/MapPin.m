//
//  MapPin.m
//  Olutu
//
//  Created by fei wang on 24/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "MapPin.h"



@implementation MapPin

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- (id)initWithCoordinates:(CLLocationCoordinate2D)location placeName:placeName description:description {
    self = [super init];
    if (self != nil) {
        coordinate = location;
        title = placeName;
        subtitle = description;
    }
    return self;
}


@end