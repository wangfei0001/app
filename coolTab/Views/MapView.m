//
//  MapView.m
//  Olutu
//
//  Created by fei wang on 5/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "MapView.h"

@implementation MapView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        
        [self load];
    }
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        self.delegate = self;

        [self load];
    }
    return self;
}


- (void)load{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"map130" ofType:@"html" inDirectory:@"web"];
    
    NSURL *base = [NSURL fileURLWithPath:path isDirectory:NO];
    
    [self loadRequest:[NSURLRequest requestWithURL:base]];
    [self setUserInteractionEnabled:NO];//设置用户不可修改
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
