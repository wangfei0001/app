//
//  SimpleTableCell.m
//  TableView2
//
//  Created by fei wang on 4/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SingleUserViewCell.h"

@implementation SingleUserViewCell


- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)toggleEditMode:(Boolean)editMode{
    //缩进
    
    int offset = editMode ? 4 : -4;

    
        CGRect nameLabelFrame = self.nameLabel.frame;
        CGRect imageFrame = self.thumbnailImageView.frame;
    
        [UIView animateWithDuration:0.1f
                         animations:nil
                         completion:^(BOOL finished){
                             [self.nameLabel setFrame:CGRectMake(nameLabelFrame.origin.x+offset,
                                                                 nameLabelFrame.origin.y,
                                                                 nameLabelFrame.size.width,
                                                                 nameLabelFrame.size.height)];
                             [self.thumbnailImageView setFrame:CGRectMake(imageFrame.origin.x+offset,
                                                                          imageFrame.origin.y,
                                                                          imageFrame.size.width,
                                                                          imageFrame.size.height)];
                         }];

}

@end
