//
//  ProfileView.h
//  coolTab
//
//  Created by fei wang on 22/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "User.h"

@interface ProfileView : UITableView<UITableViewDelegate,UITableViewDataSource>{
    User *user;
}

@property User *user;

@end
