//
//  LeftPanelCellView.h
//  Olutu
//
//  Created by fei wang on 7/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftPanelCellView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *image;

@property (weak, nonatomic) IBOutlet UILabel *text;

@property (weak, nonatomic) IBOutlet UIImageView *rightImage;

@end
