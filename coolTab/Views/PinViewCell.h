//
//  PinViewCell.h
//  coolTab
//
//  Created by fei wang on 10/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol pinViewDelegate//<NSObject>

-(void)imageClick:(id)sender indexPath:(NSIndexPath *)indexPath;

@end

@interface PinViewCell : UITableViewCell

- (void)initSlideRoundButton;

- (void)initToolbar;

@property (retain, nonatomic) IBOutlet UILabel *timeLabel;

@property (retain, nonatomic) IBOutlet UIImageView *pinImage;

@property (nonatomic, assign) id<pinViewDelegate> delegate;

@end
