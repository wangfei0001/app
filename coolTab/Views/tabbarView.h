//
//  tabbarView.h
//  tabbarTest
//
//  Created by Kevin Lee on 13-5-6.
//  Copyright (c) 2013年 Kevin. All rights reserved.
//

#import <UIKit/UIKit.h>


#import "FlyingButtonView.h"


@protocol tabViewDelegate<NSObject>
-(void)userListClick:(id)sender;
-(void)moveFloatView:(id)sender animate:(BOOL)animate;
-(void)createTripClick:(id)sender;
-(void)takePhotoClick:(id)sender;
-(void)flyingButtonExpand:(id)sender;
-(void)collapseDone:(id)sender;
-(void)expandDone:(id)sender;
-(void)touchBtnAtIndex:(NSInteger)index;
@end

//@protocol tabbarDelegate <NSObject>
//
//
//
//@end

@interface tabbarView : UIView <FlyingButtonViewDelegate>

- (void)reset;

@property(nonatomic,strong) UIImageView *tabbarView;
@property(nonatomic,strong) UIImageView *tabbarViewCenter;

@property(nonatomic,strong) UIButton *button_1;
@property(nonatomic,strong) UIButton *button_2;
@property(nonatomic,strong) UIButton *button_3;
@property(nonatomic,strong) UIButton *button_4;
@property(nonatomic,strong) UIButton *button_center;

//@property(nonatomic,weak) id<tabbarDelegate> delegate;

@property (nonatomic, assign) id<tabViewDelegate> delegate;

@end
