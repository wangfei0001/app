//
//  Facebook.h
//  SplitPay
//
//  Created by fei wang on 13-5-18.
//  Copyright (c) 2013年 Lin Wei. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FacebookSDK.h"

@protocol FacebookClientDelegate;

@interface FacebookClient : NSObject

- (void)getFriends;

- (BOOL)postFeed: (NSString *)message;

- (void)login;

- (void)logout;

- (void)getUserInfo;

@property (nonatomic, assign) id<FacebookClientDelegate> delegate;

@property (strong, nonatomic) FBSession *session;

@end


@protocol FacebookClientDelegate <NSObject>

@optional

- (void)getFriendsDone:(NSMutableArray *)friends;

- (void)authorisedDone:(BOOL)success;

@end