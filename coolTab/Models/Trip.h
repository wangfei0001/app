//
//  Trip.h
//  coolTab
//
//  Created by fei wang on 12/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "User.h"

@interface Trip : NSObject

@property (nonatomic) int tripId;

@property (nonatomic, retain) User *user;

+ (NSMutableArray *)loadTrips;

+ (NSMutableArray *)tripsData;

- (id)initWithTripId:(int)_tripId;

- (User *)getUser;

/***
 * 获取当前旅程的信息
 */
- (NSMutableArray *)loadPins:(int)page;
@end
