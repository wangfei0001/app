//
//  Singleton.m
//  Olutu
//
//  Created by fei wang on 13-6-10.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

static Singleton* sharedInstance = nil;


+ (Singleton*)getInstance {
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}



+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {

        if (sharedInstance == nil) {

            sharedInstance = [super allocWithZone:zone];

            return sharedInstance;

        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone*)zone {
    
    return self;
    
}

- (id)retain {

    return self;
}

- (unsigned)retainCount {

    return UINT_MAX;
}

//- (void)release {
//    
//}

- (id)autorelease {
    return self;
}

@end
