//
//  DataShareSetting.h
//  Olutu
//
//  Created by fei wang on 20/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BasicData.h"

@interface DataShareSetting : BasicData

@property (nonatomic, retain) NSNumber * facebook;

@property (nonatomic, retain) NSNumber * weibo;

@property (nonatomic, retain) NSNumber * qq;

@property (nonatomic, retain) NSNumber * twitter;

@end
