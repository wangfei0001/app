//
//  DataTrip.h
//  Olutu
//
//  Created by fei wang on 20/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DataShareSetting.h"

@interface DataTrip : BasicData

@property (nonatomic, retain) NSString *Name;

@property (nonatomic, retain) DataShareSetting *Share;        //share setting;

@property (nonatomic, retain) NSNumber *isLocked;                  //public or private;

@property (nonatomic, retain) NSNumber *isGroup;

@property (nonatomic, retain) NSMutableArray *groupMembers;

@property (nonatomic, retain) NSMutableArray *tags;

@end
