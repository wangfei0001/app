//
//  Global.m
//  coolTab
//
//  Created by fei wang on 15/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "Global.h"

@implementation Global

- (id) init{
    self = [super init];
    if(self){
        self.initialize = false;
    }
    return self;
}

@end
