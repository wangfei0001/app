//
//  XMLParser.m
//  coolTab
//
//  Created by fei wang on 31/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "XMLParser.h"

#define XML_PARSER_PHASE_NOT_START      0
#define XML_PARSER_PHASE_SERVICE        1
#define XML_PARSER_PHASE_METHOD_NAME    2
#define XML_PARSER_PHASE_RESPONSE       3
#define XML_PARSER_PHASE_STATUS         4

@implementation XMLParser{
    int parsingPhase;
}

@synthesize response;

- (id)initWithString: (NSString *)content{
    
    NSData *data=[NSData dataWithBytes:[content UTF8String] length:[content length]];
    
    self = [super initWithData:data];
    
    if(self){
        [self setShouldProcessNamespaces:NO];
        [self setShouldReportNamespacePrefixes:NO];
        [self setShouldResolveExternalEntities:NO];
        
        [self setDelegate:self];
        
        parsingPhase = XML_PARSER_PHASE_NOT_START;
    }
    
    return self;
}


- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
//    //NSLog(@"Name:%@",elementName);
//    if([elementName isEqualToString:@"Rest_Service_Service"]){
//        self.response = [RestResponse new];
//        parsingPhase = XML_PARSER_PHASE_SERVICE;
//        return;
//    }
//    if(parsingPhase == XML_PARSER_PHASE_SERVICE){
//        self.response.method = elementName;
//        parsingPhase = XML_PARSER_PHASE_METHOD_NAME;
//        return;
//    }
//    if([elementName isEqualToString:@"response"]){
////        if(elementName isEqualToString:@"response"){
////            self.response.response = e
////        }else{
////            
////        }
//        parsingPhase = XML_PARSER_PHASE_RESPONSE;
//    }else if([elementName isEqualToString:@"status"]){
//        parsingPhase = XML_PARSER_PHASE_STATUS;
//    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
//    NSLog(@"Value:%@",string);
    if(parsingPhase == XML_PARSER_PHASE_RESPONSE){
//        self.response.response = string;
    }else if(parsingPhase == XML_PARSER_PHASE_STATUS){
//        self.response.status = string;
    }
}

- (Boolean)isSuccess{
    return TRUE;
}

- (RestResponse *)getResponse{
    return self.response;
}

@end
