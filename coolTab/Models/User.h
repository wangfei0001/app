//
//  User.h
//  coolTab
//
//  Created by fei wang on 28/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject{
    
    int followers;
    
    int followings;

}

- (id)init: (NSString *)_screenName gender:(bool)_gender avatar:(NSString *)_avatar;

@property (nonatomic, retain) NSString *screenName;

@property (nonatomic, retain) NSString *avatar;

@property (nonatomic) bool gender;

@property (nonatomic) int idUser;

@end
