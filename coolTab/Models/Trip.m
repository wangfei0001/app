//
//  Trip.m
//  coolTab
//
//  Created by fei wang on 12/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "Trip.h"

#import "Api.h"

static NSMutableArray *tripsData;

@implementation Trip{
    Api *api;
}

+ (NSMutableArray *)tripsData {
    return tripsData;
}

+ (NSMutableArray *)loadTrips{
    //if(tripsData) return tripsData;
    
    Api *api = [Api new];
    
    RestResponse *response = [api getTrips];
    
    if(![response isSuccess]){
        NSLog(@"Failed to load data!");
        return nil;
    }
    
    NSMutableArray *retData = [[NSMutableArray alloc]initWithCapacity:0];
    
    for(id val in response.data) {
        [retData addObject:val];
    }
    
    tripsData = retData;
    
    return retData;
}


- (id)initWithTripId:(int)tripId{
    self = [super init];
    if(self){
        self.tripId = tripId;
    }
    return self;
}


- (NSMutableArray *)loadPins:(int)page{
    api = [Api new];
    
    NSDictionary *data = [api getTripDetail:self.tripId];
    
    NSMutableArray *retData = [[NSMutableArray alloc]init];
    

    for(id key in data) {
        id value = [data objectForKey:key];
        
        if(YES == [key isEqualToString:@"user"]){
            if(!self.user){
                self.user = [[User alloc] init:[value objectForKey:@"screenname"]
                                        gender:true
                                        avatar:[value objectForKey:@"avatar"]];
                continue;       //skip
            }
        }
        
        [retData addObject:value];
    }
    
    return retData;
}


/***
 * 返回当前的用户
 */
- (User *)getUser{
    return self.user;
}


@end
