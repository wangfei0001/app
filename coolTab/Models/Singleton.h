//
//  Singleton.h
//  Olutu
//
//  Created by fei wang on 13-6-10.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Singleton : NSObject

+ (Singleton*)getInstance;

@end
