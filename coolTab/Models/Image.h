//
//  Image.h
//  Olutu
//
//  Created by fei wang on 13-6-28.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image : NSObject

-(void)getImageInfo;

@end
