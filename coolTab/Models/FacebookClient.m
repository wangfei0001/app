//
//  Facebook.m
//  SplitPay
//
//  Created by fei wang on 13-5-18.
//  Copyright (c) 2013年 Lin Wei. All rights reserved.
//

#import "FacebookClient.h"

#import "Friend.h"

#import "FacebookSDK.h"

#import "Session.h"

@implementation FacebookClient

@synthesize session = _session;

- (id)init
{
    self = [super init];
    if(self){
        if(!self.session.isOpen){
            self.session = [[FBSession alloc] init];
            
            if (self.session.state == FBSessionStateCreatedTokenLoaded) {
                [self.session openWithCompletionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                    if (error) {
                        NSLog(@"Error: %@", [error localizedDescription]);
                    } else {
                        
                        
                    }
                    
                }];
            }
        }else{
            NSLog(@"Session opened");
        }
    }
    return self;
}


- (void)login
{
    
    //[self.session closeAndClearTokenInformation];
    if (self.session.state != FBSessionStateCreated) {
        // Create a new, logged out session.
        self.session = [[FBSession alloc] init];
    }
    
    [self getUserInfo];
    
    //    return;
    //
    //    // if the session isn't open, let's open it now and present the login UX to the user
    //    [self.session openWithCompletionHandler:^(FBSession *session,
    //                                                     FBSessionState status,
    //                                                     NSError *error) {
    //        // and here we make sure to update our UX according to the new session state
    //
    //    }];
}

//- (void)openSessionWithPermission: (BOOL)firstTime
//{
//    if (!FBSession.activeSession.isOpen) {
//        // if the session is closed, then we open it here, and establish a handler for state changes
//
//        NSArray *permissions = [NSArray arrayWithObjects:@"publish_actions", nil];
//
//        [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//            if (error) {
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                    message:error.localizedDescription
//                                                                   delegate:nil
//                                                          cancelButtonTitle:@"OK"
//                                                          otherButtonTitles:nil];
//                [alertView show];
//                NSLog(@"Error: %@", [error localizedDescription]);
//
//                if(firstTime) [self.delegate authorisedDone: NO];
//            } else if (session.isOpen) {
//                if(firstTime){
//                    [self sessionStateChanged:session
//                                        state:status
//                                        error:error];
//                }
//            }
//        }];
//    }
//}


- (void)getUserInfo
{
    //    if(FBSession.activeSession.isOpen){
    //        [self _getUserInfo];
    //    }else{
    //NSArray *permissions =
    //[NSArray arrayWithObjects:@"email", nil];
    
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:^(FBSession *session,
                                                      FBSessionState status,
                                                      NSError *error) {
                                      // if login fails for any reason, we alert
                                      if (error) {
                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                          message:error.localizedDescription
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"OK"
                                                                                otherButtonTitles:nil];
                                          [alert show];
                                          // if otherwise we check to see if the session is open, an alternative to
                                          // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                          // property of the session object; the macros are useful, however, for more
                                          // detailed state checking for FBSession objects
                                      } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                          // send our requests if we successfully logged in
                                          
                                          [self _getUserInfo];
                                          
                                      }
                                  }];
    //    }
}

- (void)_getUserInfo
{
    [FBRequestConnection
     startForMeWithCompletionHandler:^(FBRequestConnection *connection,
                                       id<FBGraphUser> user,
                                       NSError *error) {
         if (!error) {
             Config *config = [Session instance].config;
             
             /*
              birthday property
              first_name property
              id property
              last_name property
              link property
              location property
              middle_name property
              name property
              username property
              
              */
             config.facebook_id = user.id;
             
             config.fname = [NSString stringWithFormat:@"%@", user.first_name];
             
             config.lname = [NSString stringWithFormat:@"%@", user.last_name];
             
             NSString *userInfo = @"";
             
             
             
             // Example: typed access (name)
             // - no special permissions required
             userInfo = [userInfo
                         stringByAppendingString:
                         [NSString stringWithFormat:@"Name: %@\n\n",
                          user.name]];
             
             //                 // Example: typed access, (birthday)
             //                 // - requires user_birthday permission
             //                 userInfo = [userInfo
             //                             stringByAppendingString:
             //                             [NSString stringWithFormat:@"Birthday: %@\n\n",
             //                              user.birthday]];
             
             // Example: partially typed access, to location field,
             // name key (location)
             // - requires user_location permission
             userInfo = [userInfo
                         stringByAppendingString:
                         [NSString stringWithFormat:@"Location: %@\n\n",
                          [user.location objectForKey:@"name"]]];
             
             // Example: access via key (locale)
             // - no special permissions required
             userInfo = [userInfo
                         stringByAppendingString:
                         [NSString stringWithFormat:@"Locale: %@\n\n",
                          [user objectForKey:@"locale"]]];
             
             // Example: access via key for array (languages)
             // - requires user_likes permission
             if ([user objectForKey:@"languages"]) {
                 NSArray *languages = [user objectForKey:@"languages"];
                 NSMutableArray *languageNames = [[NSMutableArray alloc] init];
                 for (int i = 0; i < [languages count]; i++) {
                     [languageNames addObject:[[languages
                                                objectAtIndex:i]
                                               objectForKey:@"name"]];
                 }
                 userInfo = [userInfo
                             stringByAppendingString:
                             [NSString stringWithFormat:@"Languages: %@\n\n",
                              languageNames]];
             }
             
             [self.delegate authorisedDone: YES];
         }else{
             NSLog(@"Error: %@", [error localizedDescription]);
             [self.delegate authorisedDone: NO];
         }
     }];
}


- (BOOL)postFeed: (NSString *)message
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   message, @"message",
                                   nil];
    
    [FBRequestConnection startWithGraphPath:@"me/feed"
                                 parameters:params
                                 HTTPMethod:@"POST"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (error) {
                                  NSLog(@"Error: %@", [error localizedDescription]);
                              } else {
                                  
                                  
                              }
                          }];
    return YES;
}


- (void)_getFriends
{
    NSString *query =
    @"SELECT uid, name, pic_square, first_name, last_name FROM user WHERE uid IN "
    @"(SELECT uid2 FROM friend WHERE uid1 = me()) order by name";
    // Set up the query parameter
    NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:query, @"q", nil];
    // Make the API request that uses FQL
    [FBRequestConnection startWithGraphPath:@"/fql"
                                 parameters:queryParam
                                 HTTPMethod:@"GET"
                          completionHandler:^(FBRequestConnection *connection,
                                              id result,
                                              NSError *error) {
                              if (error) {
                                  NSLog(@"Error: %@", [error localizedDescription]);
                              } else {
                                  result = [result objectForKey:@"data"];
                                  
                                  NSMutableArray *fbFriendsData = [[NSMutableArray alloc] initWithCapacity:[result count]];
                                  
                                  for (int i = 0; i < [result count]; i++){
                                      id fbFriendData = [result objectAtIndex:i];
                                      Friend *friend = [Friend alloc];
                                      
                                      friend.fbName =   [fbFriendData objectForKey:@"name"];
                                      friend.fbId   =   [fbFriendData objectForKey:@"uid"];
                                      friend.firstName   =   [fbFriendData objectForKey:@"first_name"];
                                      friend.lastName    =   [fbFriendData objectForKey:@"last_name"];
                                      friend.avatar      =   [fbFriendData objectForKey:@"pic_square"];
                                      
                                      [fbFriendsData addObject:friend];
                                  }
                                  [self.delegate getFriendsDone:fbFriendsData];
                                  
                              }
                          }];
    
    
}

- (void)getFriends
{
    if(FBSession.activeSession.isOpen){
        [self _getFriends];
    }else{
        
        [FBSession openActiveSessionWithReadPermissions:nil
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session,
                                                          FBSessionState status,
                                                          NSError *error) {
                                          // if login fails for any reason, we alert
                                          if (error) {
                                              UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                              message:error.localizedDescription
                                                                                             delegate:nil
                                                                                    cancelButtonTitle:@"OK"
                                                                                    otherButtonTitles:nil];
                                              [alert show];
                                              // if otherwise we check to see if the session is open, an alternative to
                                              // to the FB_ISSESSIONOPENWITHSTATE helper-macro would be to check the isOpen
                                              // property of the session object; the macros are useful, however, for more
                                              // detailed state checking for FBSession objects
                                          } else if (FB_ISSESSIONOPENWITHSTATE(status)) {
                                              // send our requests if we successfully logged in
                                              
                                              [self _getFriends];
                                              
                                          }
                                      }];
    }
    //if (!FBSession.activeSession.isOpen) {
    
    //    }else{
    //        NSLog(@"Faild inactive session");
    //    }
}

- (void)logout
{
    if([self.session isOpen]){
        [self.session closeAndClearTokenInformation];
        
        //    [self.session close];
        //    [FBSession setActiveSession:nil];
        NSLog(@"clear token");
    }
}


@end
