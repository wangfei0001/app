//
//  User.m
//  coolTab
//
//  Created by fei wang on 28/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "User.h"

#define URL_USER_AVATAR @"http://photo.olutu.com/avatar/"

@implementation User


/***
 *
 */
- (id)init: (NSString *)screenName gender:(bool)gender avatar:(NSString *)avatar
{
    self = [super init];
    if(self){
        self.screenName = screenName;
        self.gender = gender;
        //NSString *url = URL_USER_AVATAR;
        self.avatar = [URL_USER_AVATAR stringByAppendingString:avatar];
        NSLog(@"%@", self.avatar);
    }
    return self;
}

@end
