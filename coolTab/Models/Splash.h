//
//  Splash.h
//  Olutu
//
//  Created by fei wang on 13-6-15.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Splash : NSObject

- (NSArray *)load;

@end
