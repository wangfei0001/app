//
//  Database.m
//  Olutu
//
//  Created by fei wang on 25/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "Database.h"

#import "DbTrip.h"

@implementation Database

static Database *_database;


+ (Database *)database {
    if (_database == nil) {
        _database = [[Database alloc] init];
    }
    return _database;
}


- (NSString *)dataFilePath:(BOOL)forSave {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *documentsPath = [documentsDirectory stringByAppendingPathComponent:@"olutu.sqlite"];
    if (forSave || [[NSFileManager defaultManager] fileExistsAtPath:documentsPath]) {
        return documentsPath;
    } else {
        return [[NSBundle mainBundle] pathForResource:@"olutu" ofType:@"sqlite"];
    }
}

- (id)init{
    self = [super init];
    if(self){
        NSString *filePath = [self dataFilePath:YES];
        NSLog(@"Path:%@", filePath);
        if(![[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            NSError *error;
            NSString *bundleFilePath = [self dataFilePath:NO];
            [[NSFileManager defaultManager] copyItemAtPath:bundleFilePath toPath:filePath error:&error];
            //TODO error handling
            if(error){
                
            }
        }
        
        if (sqlite3_open([filePath UTF8String], &_database) != SQLITE_OK) {
            NSLog(@"Failed to open database!");
            //error handling
        }
    }
    return self;
}


- (NSArray *)getTrips{
    NSMutableArray *retval = [[NSMutableArray alloc] init];
    NSString *query = @"SELECT id_trip, trip_name, is_uploaded, created_at, updated_at FROM trip ORDER BY created_at DESC";
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK) {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            int id_trip = sqlite3_column_int(statement, 0);
            char *trip_name = (char *) sqlite3_column_text(statement, 1);
            int is_uploaded = sqlite3_column_int(statement, 2);
            char *created_at = (char *) sqlite3_column_text(statement, 3);
            char *updated_at = (char *) sqlite3_column_text(statement, 4);
            
            DbTrip *info = [[DbTrip alloc] init];
            
            info.id_trip = id_trip;
            info.trip_name = [[NSString alloc] initWithUTF8String:trip_name];
            info.is_uploaded = is_uploaded;
            if(created_at) info.created_at = [[NSString alloc] initWithUTF8String:created_at];
            if(updated_at) info.updated_at = [[NSString alloc] initWithUTF8String:updated_at];
            
            [retval addObject:info];
        }
        sqlite3_finalize(statement);
    }
    return retval;
}

- (void)dealloc{
    sqlite3_close(_database);
}

@end
