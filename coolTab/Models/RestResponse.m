//
//  RestResponse.m
//  coolTab
//
//  Created by fei wang on 4/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "RestResponse.h"

@implementation RestResponse

-(id) initWithData: (NSString *)jsonString
{
    self = [super init];
    if(self){
        NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];

        NSError *error = nil;
        NSLog(@"%@", jsonString);
        id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
        if (jsonObject != nil && error == nil){
            NSLog(@"Successfully deserialized...");
            if ([jsonObject isKindOfClass:[NSDictionary class]]){
                NSDictionary *deserializedDictionary = (NSDictionary *)jsonObject;
                //self.data = deserializedDictionary;
                self.status = [[deserializedDictionary objectForKey:@"status"] boolValue];
                self.data = [deserializedDictionary objectForKey:@"data"];
                self.message = [deserializedDictionary objectForKey:@"message"];
                
                
//            } else if ([jsonObject isKindOfClass:[NSArray class]]){
//                NSArray *deserializedArray = (NSArray *)jsonObject;
//                NSLog(@"Dersialized JSON Array = %@", deserializedArray);
//                
//                return deserializedArray;
            } else {
                NSLog(@"An error happened while deserializing the JSON data.");
            }
        }
    }
    return self;
}

- (BOOL)isSuccess
{
    return self.status;
}

@end
