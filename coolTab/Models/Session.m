//
//  Session.m
//  Olutu
//
//  Created by fei wang on 13-6-10.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "Session.h"

@implementation Session


static Session *shareInstance;

+ (Session*)instance
{
    if (shareInstance == nil)
    {
        shareInstance = [[Session alloc] init];
    }
    return shareInstance;
}

- (BOOL)isGuest
{
    return NO;
}

@end
