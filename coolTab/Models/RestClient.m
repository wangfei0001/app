//
//  RestClient.m
//  coolTab
//
//  Created by fei wang on 16/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import "RestClient.h"

@interface RestClient(){
    bool finished;
    
    NSString *boundary;
}

@property (nonatomic, retain) NSMutableData *receivedData;

@end


@implementation RestClient


- (id) init{
    self = [super init];
    if(self){
        //self.request = [NSURLRequest init];
        self.method = HTTP_CLIENT_METHOD_GET;
        self.url = @"";
        self.timeout = 60;
        
        finished = NO;
        
    }
    return self;
}

- (id) initRequest: (NSString *)url
            method: (NSString *)method
            timeout: (int) timeout{
    self.url = url;
    self.method = method;
    self.timeout = timeout;
    return self;
}


- (NSString *)getBoundary{
    return @"0194784892923";
}

//Send request
- (BOOL) sendRequest{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.url]
        cachePolicy:NSURLRequestUseProtocolCachePolicy
        timeoutInterval:self.timeout
     ];
    
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(connection){
        self.receivedData = [NSMutableData data];
        
        while(!finished) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        
        return YES;
    }
    return NO;
}


- (BOOL) sendPostRequest: (NSMutableData *)data{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.url]
                                             cachePolicy:NSURLRequestUseProtocolCachePolicy
                                         timeoutInterval:self.timeout
                             ];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", [self getBoundary]];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setHTTPShouldHandleCookies:NO];
    
    [request setHTTPMethod:self.method];
    
    [request setHTTPBody:data];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [data length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if(connection){
        self.receivedData = [NSMutableData data];
        
        while(!finished) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        
        return YES;
    }
    return NO;
}



- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    [self.receivedData setLength:0];
}


- (void) connection: (NSURLConnection *)connection didReceiveData:(NSData *)data{
    [self.receivedData appendData:data];
    NSString* returnString= [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
    
    
    NSLog(@"%@", returnString);
}

- (void) connection: (NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    finished = YES;
    
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@"Errore recuperando la Location"
//                          message:[error localizedDescription]
//                          delegate:nil
//                          cancelButtonTitle:@"Okay"
//                          otherButtonTitles:nil];
//    [alert show];
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    finished = YES;
}

- (NSString *)getData{
    return [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];

}

@end
