//
//  Image.m
//  Olutu
//
//  Created by fei wang on 13-6-28.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "Image.h"

#import <imageIo/CGImageSource.h>

#import <ImageIO/CGImageProperties.h>

#import <CoreFoundation/CFData.h>

#import<QuartzCore/QuartzCore.h>


@implementation Image


-(void)getImageInfo
{
    NSString *myPath = [[NSBundle mainBundle] pathForResource:@"IMG_0998" ofType:@"JPG"];
    NSURL *myURL = [NSURL fileURLWithPath:myPath];
    CGImageSourceRef mySourceRef = CGImageSourceCreateWithURL((CFURLRef)myURL, NULL);
    NSDictionary *myMetadata = (NSDictionary *) CGImageSourceCopyPropertiesAtIndex(mySourceRef,0,NULL);
    NSDictionary *exifDic = [myMetadata objectForKey:(NSString *)kCGImagePropertyExifDictionary];
    NSDictionary *tiffDic = [myMetadata objectForKey:(NSString *)kCGImagePropertyTIFFDictionary];
    NSLog(@"exifDic properties: %@", myMetadata); //all data
    float rawShutterSpeed = [[exifDic objectForKey:(NSString *)kCGImagePropertyExifExposureTime] floatValue];
    int decShutterSpeed = (1 / rawShutterSpeed);
    NSLog(@"Camera %@",[tiffDic objectForKey:(NSString *)kCGImagePropertyTIFFModel]);
    NSLog(@"Focal Length %@mm",[exifDic objectForKey:(NSString *)kCGImagePropertyExifFocalLength]);
    NSLog(@"Shutter Speed %@", [NSString stringWithFormat:@"1/%d", decShutterSpeed]);
    NSLog(@"Aperture f/%@",[exifDic objectForKey:(NSString *)kCGImagePropertyExifFNumber]);
    NSNumber *ExifISOSpeed  = [[exifDic objectForKey:(NSString*)kCGImagePropertyExifISOSpeedRatings] objectAtIndex:0];
    NSLog(@"ISO %i",[ExifISOSpeed integerValue]);
    NSLog(@"Taken %@",[exifDic objectForKey:(NSString*)kCGImagePropertyExifDateTimeDigitized]);
    
    NSLog(@"GEO %@",[myMetadata objectForKey:(NSString*)kCGImagePropertyGPSDictionary]);
//    NSData* pngData =  UIImagePNGRepresentation(pImage);
//    
//    CGImageSourceRef source = CGImageSourceCreateWithData((CFDataRef)pngData, NULL);
//    NSDictionary *metadata = (NSDictionary *) CGImageSourceCopyPropertiesAtIndex(source, 0, NULL);
//    
//    NSMutableDictionary *metadataAsMutable = [[metadata mutableCopy]autorelease];
//    [metadata release];
//    
//    //For GPS Dictionary
//    NSMutableDictionary *GPSDictionary = [[[metadataAsMutable objectForKey:(NSString *)kCGImagePropertyGPSDictionary]mutableCopy]autorelease];
//    if(!GPSDictionary)
//        GPSDictionary = [NSMutableDictionary dictionary];
//    
//    [GPSDictionary setValue:[NSNumber numberWithDouble:currentLatitude] forKey:(NSString*)kCGImagePropertyGPSLatitude];
//    [GPSDictionary setValue:[NSNumber numberWithDouble:currentLongitude] forKey:(NSString*)kCGImagePropertyGPSLongitude];
//    
//    NSString* ref;
//    if (currentLatitude <0.0)
//        ref = @"S";
//    else
//        ref =@"N";
//    [GPSDictionary setValue:ref forKey:(NSString*)kCGImagePropertyGPSLatitudeRef];
//    
//    if (currentLongitude <0.0)
//        ref = @"W";
//    else
//        ref =@"E";
//    [GPSDictionary setValue:ref forKey:(NSString*)kCGImagePropertyGPSLongitudeRef];
//    
//    [GPSDictionary setValue:[NSNumber numberWithFloat:location.altitude] forKey:(NSString*)kCGImagePropertyGPSAltitude];
//    
//    //For EXIF Dictionary
//    NSMutableDictionary *EXIFDictionary = [[[metadataAsMutable objectForKey:(NSString *)kCGImagePropertyExifDictionary]mutableCopy]autorelease];
//    if(!EXIFDictionary)
//        EXIFDictionary = [NSMutableDictionary dictionary];
//    
//    [EXIFDictionary setObject:[NSDate date] forKey:(NSString*)kCGImagePropertyExifDateTimeOriginal];
//    [EXIFDictionary setObject:[NSDate date] forKey:(NSString*)kCGImagePropertyExifDateTimeDigitized];
//    
//    //add our modified EXIF data back into the image’s metadata
//    [metadataAsMutable setObject:EXIFDictionary forKey:(NSString *)kCGImagePropertyExifDictionary];
//    [metadataAsMutable setObject:GPSDictionary forKey:(NSString *)kCGImagePropertyGPSDictionary];
//    
//    CFStringRef UTI = CGImageSourceGetType(source);
//    
//    NSMutableData *dest_data = [NSMutableData data];
//    CGImageDestinationRef destination = CGImageDestinationCreateWithData((CFMutableDataRef)dest_data, UTI, 1, NULL);
//    
//    if(!destination)
//        dest_data = [[pngData mutableCopy] autorelease];
//    else
//    {
//        CGImageDestinationAddImageFromSource(destination, source, 0, (CFDictionaryRef) metadataAsMutable);
//        BOOL success = CGImageDestinationFinalize(destination);
//        if(!success)
//            dest_data = [[pngData mutableCopy] autorelease];
//    }
//    
//    if(destination)
//        CFRelease(destination);
//    
//    CFRelease(source);
//    
//    return dest_data;
}

@end
