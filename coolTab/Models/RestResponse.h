//
//  RestResponse.h
//  coolTab
//
//  Created by fei wang on 4/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestResponse : NSObject{

}

@property (nonatomic, strong) NSDictionary *data;

@property (nonatomic, assign) BOOL status;

@property (nonatomic, strong) NSString *message;

@property (nonatomic, assign) int code;

-(id) initWithData: (NSString *)jsonString;

- (BOOL)isSuccess;

@end
