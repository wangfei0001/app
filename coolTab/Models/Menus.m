//
//  Menus.m
//  Olutu
//
//  Created by fei wang on 8/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//



#import "Menus.h"

#import "XMLReader.h"

@implementation Menus


/***
 * Load left menu from xml file
 */
+ (NSMutableArray *)getLeftMenu{
    NSError *errorPointer = nil;
    
    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:@"leftMenu" ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    
    NSDictionary *dic = [XMLReader dictionaryForXMLData:xmlData error:&errorPointer];
    
    id root = [dic objectForKey:@"root"];
    
    NSMutableArray *result = [[NSMutableArray alloc]initWithCapacity:0];
    
    for(id key in root) {
        NSMutableArray *sections = [root objectForKey:key];
        
        for(int i = 0; i < sections.count; i++){
            NSMutableArray *menus = [[sections[i] objectForKey:@"menus"] objectForKey:@"menu"];
            NSString *title = [sections[i] objectForKey:@"title"];
            
            [result addObject:[[NSArray alloc]initWithObjects:title, menus,nil]];
        }
    }
    return result;
    
}

@end
