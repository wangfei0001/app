//
//  XMLParser.h
//  coolTab
//
//  Created by fei wang on 31/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RestResponse.h"

@interface XMLParser : NSXMLParser<NSXMLParserDelegate>{
 
@private
    RestResponse *response;
    
}

@property (retain) RestResponse *response;

- (id)initWithString: (NSString *)content;

- (Boolean)isSuccess;

- (RestResponse *)getResponse;

@end
