//
//  XMLParser2.m
//  coolTab
//
//  Created by fei wang on 2/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "XMLParser2.h"

@implementation XMLParser2


- (id)initWithData:(NSData *)data{
    self = [super initWithData:data];
    if(self){
        [self setShouldProcessNamespaces:NO];
        [self setShouldReportNamespacePrefixes:NO];
        [self setShouldResolveExternalEntities:NO];
        
        [self setDelegate:self];
    }
    return self;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    NSLog(@"Name:%@ QName:%@",elementName,qName);
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{

    NSLog(@"Value:%@",string);

}

@end
