//
//  Api.m
//  coolTab
//
//  Created by fei wang on 30/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "Api.h"

#import "RestClient.h"

#import <objc/runtime.h>

#import "XMLParser.h"

//#import "AFJSONRequestOperation.h"

//#define BASE_URL @"http://back.olutu.com/rest/api/"

#define BASE_URL @"http://olutu-yii/api/v1/"

#define API_URL_GET_FOLLOWERS @"index?method=getFollowers&userid=%d&page=%d"

#define API_URL_GET_TRIPS @"index?method=getTrips&page=%d"

#define API_URL_GET_TRIP_DETAIL @"index?method=getTripDetail&page=%d"

#define API_URL_SAVE_TRIP @"index?method=saveTrip"

#define API_URL_GET_TRIP_BY_USER @"index?method=getTripByUser&userid=%d&page=%d"




@implementation Api{
    RestClient *_client;
}

- (id)init{
    self = [super init];
    
    if(self){
        _client = [RestClient new];
        [_client setTimeout:60];
        
        self.response = [RestResponse alloc];
        
    }
    return self;
}


/***
 * 获取API命令的链接，根据Base URL生成，可以改进为使用宏定义
 */
- (NSString *)getCommandUrl:(NSString *)command{
    return [BASE_URL stringByAppendingString:command];
}


/***
 * Send post request
 */
- (NSDictionary *) sendPostRequest:(NSString *)command data:(NSMutableData *)data{
    NSString *commandUrl = [self getCommandUrl: command];
    
    [_client setUrl:commandUrl];
    [_client setMethod:@"POST"];
    [_client setTimeout:60];
    
    
    if([_client sendPostRequest:data] == TRUE){
        NSString *xml = [_client getData];
        
        XMLParser *parser = [[XMLParser alloc]initWithString:xml];
        [parser parse];
        Boolean result = [parser isSuccess];
        if(result){
            RestResponse *response = [parser getResponse];
            
            return nil;//[response getData];
        }
        
    }
    return nil;
}

/***
 * 发送命令并将结果生成Json Data，如果失败则返回Nil
 */
//-(NSDictionary *)sendRequest:(NSString *)command{
//    NSString *commandUrl = [self getCommandUrl: command];
//    
//    [_client setUrl:commandUrl];
//    
//    if([_client sendRequest] == TRUE){
//        NSString *xml = [_client getData];
//        
//        XMLParser *parser = [[XMLParser alloc]initWithString:xml];
//        [parser parse];
//        Boolean result = [parser isSuccess];
//        if(result){
//            RestResponse *response = [parser getResponse];
//            
//            return [response getData];
//        }
//        
//    }
//    return nil;
//}

-(RestResponse *)sendRequestJson:(NSString *)command{
    //NSString *commandUrl = [self getCommandUrl: command];
    [_client setUrl:command];
    
    if([_client sendRequest] == TRUE){
        return [[RestResponse alloc] initWithData:[_client getData]];
    }
    return nil;
}

/***
 * 获取首页面的行程列表
 */
- (RestResponse *)getTrips{
    NSString *command = [NSString stringWithFormat:API_URL_GET_TRIPS, 1];

//    NSDictionary *data = [self sendRequest:command];

//    NSLog(@"Dersialized JSON Dictionary = %@", data);
    
    return [self sendRequestJson:@"http://olutu-yii/api/v1/trips"];
}



/***
 * 获取用户的粉丝
 **/
- (NSDictionary *)getFollowers:(int)userid{
    NSString *command = [NSString stringWithFormat:API_URL_GET_FOLLOWERS, 1, 1];
    
//    NSDictionary *data = [self sendRequest:command];
    
//    NSLog(@"Dersialized JSON Dictionary = %@", data);

    return nil;//data;
}


/***
 * 获取某个行程的所有记录
 */
- (NSDictionary *)getTripDetail:(int)tripId{
    NSString *command = [NSString stringWithFormat:API_URL_GET_TRIP_DETAIL, 1];
    
//    NSDictionary *data = [self sendRequest:command];
    
//    NSLog(@"Dersialized JSON Dictionary = %@", data);
    
    return nil;//data;
}


/*
 * 获取某个人的行程
 */
- (NSDictionary *)getTripByUser:(int)userId page:(int)page{
    NSString *command = [NSString stringWithFormat:API_URL_GET_TRIP_BY_USER, userId, page];
    
//    NSDictionary *data = [self sendRequest:command];
        
    return nil;//data;
}


/*
 * 创建或修改行程
 */
- (NSDictionary *)saveTrip:(id)trip{
    
    NSMutableData *body = [[NSMutableData alloc] init];
    
    body = [self generatePostData:body obj:trip];
    
    NSDictionary *data = [self sendPostRequest:API_URL_SAVE_TRIP data:body];
    
    return data;
}


/*
 *  生成post数据
 */
- (NSMutableData *)generatePostData:(NSMutableData *)data obj:(id)obj{
    
    NSString *boundaryConstant = [_client getBoundary];
    
    unsigned propertyCount;
    
    objc_property_t *properties = class_copyPropertyList([obj class], &propertyCount);
    
    for (int i = 0; i < propertyCount; i++) {
        objc_property_t property = properties[i];

        NSString *propName = [NSString stringWithUTF8String:property_getName(property)];
        
        id propertyValue = [obj valueForKey:propName];
        
        //含有子对象
        if([propertyValue isKindOfClass:[BasicData class]]){     
            NSMutableData *subbody = [[NSMutableData alloc] init];
            
            subbody = [self generatePostData:subbody obj:propertyValue];
            
            [data appendData:subbody];
            
            continue;
        }
        //处理数组
        if([propertyValue isKindOfClass:[NSMutableArray class]]){
            NSMutableArray *array = (NSMutableArray *)propertyValue;
            for(int i = 0; i < array.count; i++){
                [data appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [data appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@[]\"\r\n\r\n", propName] dataUsingEncoding:NSUTF8StringEncoding]];
                
                [data appendData:[[NSString stringWithFormat:@"%@", [array objectAtIndex:i]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            continue;
        }
        
        [data appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [data appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", propName] dataUsingEncoding:NSUTF8StringEncoding]];
        
        if([propertyValue isKindOfClass:[NSString class]]){
            [data appendData:[[NSString stringWithFormat:@"%@", propertyValue] dataUsingEncoding:NSUTF8StringEncoding]];
            
        }else if([propertyValue isKindOfClass:[NSNumber class]]){
            [data appendData:[[NSString stringWithFormat:@"%d", [propertyValue intValue]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }

    return data;
}


- (RestResponse *)getSplashImages
{
    NSString *command = @"http://olutu-yii/api/v1/splash";
    
    return [self sendRequestJson:command];
}


+ (void)call: (NSString *)url
      method: (NSString *)method
        param: (NSDictionary *)param
        success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success
{
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:[NSURL URLWithString:DOMAIN_URL]];

    NSString *path = [NSString stringWithFormat:@"%@%@", BASE_URL, url];
    
    NSMutableURLRequest *request = [httpClient requestWithMethod:method path:path parameters:param];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
        success: success
        failure:^(NSURLRequest *request , NSURLResponse *response , NSError *error , id JSON) {
            
            NSLog(@"%@", error);
            UIAlertView *alert =[[UIAlertView alloc] initWithTitle:@"失败"
                                                           message:@"您的网络有点问题哦"
                                                          delegate:NULL
                                                 cancelButtonTitle:@"确定"
                                                 otherButtonTitles:NULL];
            [alert show];
        }];
    [operation start];

}






//- (NSMutableData *)appendString:(NSMutableData *)body str:(NSString *)str{
//    [body appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
//    return body;
//}

@end
