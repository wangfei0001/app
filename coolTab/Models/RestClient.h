//
//  RestClient.h
//  coolTab
//
//  Created by fei wang on 16/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HTTP_CLIENT_METHOD_GET @"GET";

#define HTTP_CLIENT_METHOD_POST @"POST";


@interface RestClient : NSURLConnection

- (id) initRequest: (NSString *)url
            method: (NSString *)method
           timeout: (int) timeout;

- (BOOL) sendRequest;

- (BOOL) sendPostRequest: (NSMutableData *)data;

- (NSString *)getData;

- (NSString *)getBoundary;

@property (nonatomic) NSString *url;

@property (nonatomic) int timeout;

@property (nonatomic) NSString *method;


@end
