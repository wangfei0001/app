//
//  Session.h
//  Olutu
//
//  Created by fei wang on 13-6-10.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Singleton.h"

#import "Config.h"

@interface Session : NSObject

+ (Session*)instance;

- (BOOL)isGuest;

@property (nonatomic, strong) Config *config;

@property (nonatomic, strong) NSMutableArray *tripsData;


@end
