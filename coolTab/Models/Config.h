//
//  Config.h
//  Olutu
//
//  Created by fei wang on 13-6-10.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

@property int uid;

//@property (strong, nonatomic) NSString *private_key;

@property (strong, nonatomic) NSString *avatar;

@property (strong, nonatomic) NSString *session_id;

@property (strong, nonatomic) NSString *expired;        //login expired date

@property (strong, nonatomic) NSString *facebook_id;

@property (strong, nonatomic) NSString *webo_id;

@property (strong, nonatomic) NSString *fname;

@property (strong, nonatomic) NSString *lname;

@end
