//
//  Global.h
//  coolTab
//
//  Created by fei wang on 15/02/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "Singleton.h"

//#define ENABLE_SPLASH_IMAGES 1

/** Sina Weibo 将下面注释取消，并定义自己的app key，app secret以及授权跳转地址uri
 此demo即可编译运行**/

#define kAppKey             @"1108221564"
#define kAppSecret          @"137cfa1b91537d5363978e6d2f5f9b40"
#define kAppRedirectURI     @"http://www.sina.com"


#ifndef kAppKey
#error
#endif

#ifndef kAppSecret
#error
#endif

#ifndef kAppRedirectURI
#error
#endif

@interface Global : NSObject{

}


@property (nonatomic) bool initialize;

@property (retain, nonatomic) NSMutableArray *tripData;

@end
