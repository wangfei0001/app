//
//  Splash.m
//  Olutu
//
//  Created by fei wang on 13-6-15.
//  Copyright (c) 2013年 fei wang. All rights reserved.
//

#import "Splash.h"

#import "XMLReader.h"

#import "SDImageCache.h"

#import "Api.h"

@implementation Splash


- (NSArray *) load
{
    Api *api = [[Api alloc] init];
    
    RestResponse *response = [api getSplashImages];
    
    if([response isSuccess]){
    
//        NSError *errorPointer = nil;
//        
        NSMutableArray *splashImages, *splashDurations;
        
        splashImages = [[NSMutableArray alloc]initWithCapacity:0];
        splashDurations = [[NSMutableArray alloc]initWithCapacity:0];
//        
//        NSString *xmlPath = [[NSBundle mainBundle] pathForResource:@"cover" ofType:@"xml"];
//        NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
//        
//        NSDictionary *dic = [XMLReader dictionaryForXMLData:xmlData error:&errorPointer];
//        
//        id root = [dic objectForKey:@"root"];
//        for(id key in [response data]) {
            //root/image is NSMutableArray
        NSDictionary *data = response.data;
        //NSMutableArray *images = [data objectForKey:key];
        
        SDImageCache *cache = [[SDImageCache alloc]init];
        
        for(id imgData in data){
            //NSString *type = [images[i] objectForKey:@"@type"];
            float duration = [[imgData objectForKey:@"duration"] floatValue];
            NSString *url = [imgData objectForKey:@"url"];
            
            NSLog(@"url=%@ duration=%f", url, duration);
            
            
            NSURL *imgURL=[[NSURL alloc]initWithString:url];
            
            //使用__block后才可以在Block中修改变量
            __block UIImage *cacheImage;
            
            [cache queryDiskCacheForKey:imgURL.absoluteString done:^(UIImage *image, SDImageCacheType cacheType){
                cacheImage = image;
            }];
            
            if(!cacheImage){
                NSData *imgdata=[[NSData alloc]initWithContentsOfURL:imgURL];
                UIImage *image=[[UIImage alloc]initWithData:imgdata];
                
                [cache storeImage:image forKey:imgURL.absoluteString toDisk:YES];
                cacheImage = image;
            }
            if(cacheImage){
                [splashImages addObject:cacheImage];
                [splashDurations addObject:[NSNumber numberWithInt:duration]];
            }
        }
        return [[NSArray alloc]initWithObjects:splashImages,splashDurations, nil];
//        }
    }
    return nil;
}

@end
