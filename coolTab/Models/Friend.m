//
//  Friend.m
//  SplitPay
//
//  Created by Lin Wei on 13-5-14.
//  Copyright (c) 2013年 Lin Wei. All rights reserved.
//

#import "Friend.h"

@implementation Friend

- (NSString*)paidDataKey
{
    NSString *name = [self.firstName stringByReplacingOccurrencesOfString:@"," withString:@" "];
    name = [self.firstName stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    
    if (self.fbId > 0)
    {
        return [NSString stringWithFormat:@"%@,%@", self.fbId, name];
    }
    return [NSString stringWithFormat:@"%d,%@", self.friendId, name];
}


- (BOOL)isEqualFriend:(Friend *)friend
{
    if([[NSString stringWithFormat:@"%@",self.fbId] isEqualToString:[NSString stringWithFormat:@"%@",friend.fbId]]){
        return YES;
    }
    
    return NO;
}

@end
