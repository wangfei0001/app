//
//  Friend.h
//  SplitPay
//
//  Created by Lin Wei on 13-5-14.
//  Copyright (c) 2013年 Lin Wei. All rights reserved.
//

#import <Foundation/Foundation.h>

#define COLUMN_INDEX_FRIEND_ID 0
#define COLUMN_INDEX_FRIEND_FNAME 1
#define COLUMN_INDEX_FRIEND_LNAME 2
#define COLUMN_INDEX_FRIEND_EMAIL 3
#define COLUMN_INDEX_FRIEND_FBNAME 4
#define COLUMN_INDEX_FRIEND_WBNAME 5
#define COLUMN_INDEX_FRIEND_AVATAR 6
#define COLUMN_INDEX_FRIEND_FBID 7
#define COLUMN_INDEX_FRIEND_WBID 8

@interface Friend : NSObject

@property int friendId;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSString *avatar;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *fbName;
@property (strong, nonatomic) NSString *wbName;
@property (strong, nonatomic) NSString *fbId;
@property (strong, nonatomic) NSString *wbId;

- (NSString*)paidDataKey;


- (BOOL)isEqualFriend:(Friend *)friend;

@end
