//
//  Api.h
//  coolTab
//
//  Created by fei wang on 30/01/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Data.h"

#import "RestResponse.h"

#import "AFHTTPClient.h"

#import "AFJSONRequestOperation.h"

#define DOMAIN_URL  @"http://olutu-yii/"

#define BASE_URL    @"api/v1/"

@interface Api : NSObject


@property (nonatomic, strong) RestResponse *response;

- (NSDictionary *)getFollowers:(int)userid;

- (RestResponse *)getTrips;

- (NSDictionary *)getTripDetail:(int)tripId;

- (NSDictionary *)saveTrip:(id)trip;

- (NSDictionary *)getTripByUser:(int)userId page:(int)page;

- (RestResponse *)getSplashImages;


+ (void)call: (NSString *)url
    method: (NSString *)method
    param: (NSDictionary *)param
    success:(void (^)(NSURLRequest *request, NSHTTPURLResponse *response, id JSON))success;

@end
