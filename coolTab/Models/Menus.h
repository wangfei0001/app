//
//  Menus.h
//  Olutu
//
//  Created by fei wang on 8/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Singleton.h"

@interface Menus : Singleton


+ (NSMutableArray *)getLeftMenu;


@end
