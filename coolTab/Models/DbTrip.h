//
//  DbTrip.h
//  Olutu
//
//  Created by fei wang on 25/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DbTrip : NSObject

@property (nonatomic, assign) int id_trip;

@property (nonatomic, retain) NSString *trip_name;

@property (nonatomic, assign) int is_uploaded;

@property (nonatomic, retain) NSString *created_at;

@property (nonatomic, retain) NSString *updated_at;

@end
