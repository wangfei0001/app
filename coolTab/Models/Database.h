//
//  Database.h
//  Olutu
//
//  Created by fei wang on 25/03/13.
//  Copyright (c) 2013 fei wang. All rights reserved.
//

#import "Singleton.h"

#import <sqlite3.h>

@interface Database : Singleton{
    sqlite3 *_database;
}

+ (Database*)database;

- (NSArray *)getTrips;

@end
