//
//  FlyingButtonView.h
//  coolTab
//
//  Created by fei wang on 8/12/12.
//  Copyright (c) 2012 fei wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FlyingButtonViewDelegate;


@interface FlyingButtonView : UIImageView

- (FlyingButtonView *)initView:(NSString *)title 
                     normalImage:(UIImage *)normalImage
                    startPoint: (CGPoint) startPoint
                      endPoint: (CGPoint) endPoint
                    delegate: (id <FlyingButtonViewDelegate>) delegate;

- (void)expand:(BOOL)animation;

- (void)collapse:(BOOL)animation;


@end


@protocol FlyingButtonViewDelegate <NSObject>

- (void)flyingButtonHighlighted:(FlyingButtonView *)highlightedFlyingButton;

@end
